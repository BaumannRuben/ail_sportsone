<pre>
<?php
include 'ail-gate/AILGate.php';

// Laden aller Sportler
$option['conditions'] = array(
    'AND' => array(
        'VEREINSEINTRITT !=' => '',
        'E_VEREINSEINTRITT' => ''
    )
);
$adresses = HochwarthIT_AILGate::getAdressDao()->find('all', $option);
echo sizeOf($adresses)."<br>";
for ($i = 0; $i < sizeOf($adresses); $i ++) {
    $adress_temp = HochwarthIT_AILGate::getAdressDao()->load($adresses[$i]['GGUID']['value']);
    if($adress_temp->getValue('VEREINSEINTRITT')!="null") {
        $newDate =date("Y-m-d", strtotime($adress_temp->getValue('VEREINSEINTRITT')));
    }
    else {
        $newDate = null;
    }
    $adress_temp->setValue($newDate, 'E_VEREINSEINTRITT', 'DATETIME');
    $adress_temp = HochwarthIT_AILGate::getAdressDao()->save($adress_temp);
    echo $i.": ".$adress_temp->getValue('NAME')." ".$adress_temp->getValue('VEREINSEINTRITT')." -> ".$adress_temp->getValue('E_VEREINSEINTRITT')."<br>";
}
   