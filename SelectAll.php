<?php
include 'ail-gate/AILGate.php';
include 'SportsOneApi.php';

echo "<pre> Start";

// Neue Instanz zur Verwendung der API
$SportsOne = new SportsOneApi();
// Feste SURROGATE_CLUB_ID f�r Anpfiff ins Leben e.V.
$surrogateClubId = 'A5E0C459E4996045E10000000A793182';

$saison = $_POST['saison'];
$saison_start = substr($saison, 0, 4).'-06-01'; //zuvor -07-01
$saison_ende = substr($saison, 5, 4).'-05-31'; //zuvor -06-30
$heute = date("Y-m-d");
$gestern = date("Y-m-d", strtotime("-1 day"));

// Laden aller Standorte aus gW
$option['conditions'] = array(
    'AND' => array(
        'GWSTYPE' => 'Standort',
        'GWISCOMPANY' => 1,
        'GWISCONTACT' => 0
    )
);
$option['order'] = array(
    'COMPNAME'
);
$adresses = HochwarthIT_AILGate::getAdressDao()->find('all', $option);

// Lade freigegebene Mannschaften der aktuellen Saison f�r jeden Standort
for ($i = 0; $i < sizeOf($adresses); $i ++) {
    // TODO Feldname aus Livesystem
    $mannschaften = HochwarthIT_AILGate::getAdressDao()->getLinkedMannschaft($adresses[$i]['GGUID']['value'], 'BEREICH = \'Fussball\' AND SAISON = \''.$saison.'\' AND SPORTSONE = 1', '*', 'SAISON, NAME');
//     print_r($mannschaften); 
   
    if (! empty($mannschaften)) {
        echo "<b><u>" . $i . ": " . $adresses[$i]['COMPNAME']['value'] . "</u></b><br>";
        for ($j = 0; $j < sizeOf($mannschaften); $j ++) {
            $mannschaften[$j]['MD5GGUID']['value'] = md5($mannschaften[$j]['NAME']['value']); // Eindeutige ID f�r das Team aus Mannschaftsname mit md5
            echo $j . ": " . $mannschaften[$j]['SAISON']['value'] . " - " . $mannschaften[$j]['NAME']['value'] . " (" . $mannschaften[$j]['GGUID']['value'] . ") SURROGATE_TEAM_ID: " . $mannschaften[$j]['MD5GGUID']['value'] . "</b><br>";
            
            if ($mannschaften[$j]['GESCHLECHT']['value']==utf8_encode('m�nnlich'))
                $team_gender = 'male';
            else if ($mannschaften[$j]['GESCHLECHT']['value']=='weiblich')
                $team_gender = 'female';
            else
                $team_gender = 'mixed';
                
            echo $team_gender." - ".$mannschaften[$j]['GESCHLECHT']['value']."<br>";
            // Mannschaften in SportsOne anlegen
            $team_temp = json_encode(array(
                "PROVIDER_ID" => "CAS",
                "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                "SURROGATE_CLUB_ID" => $surrogateClubId,
                "SPORTS_TYPE" => "Soccer",
                "NAME" => $mannschaften[$j]['NAME']['value'],
                "SHORT_NAME" => "",
                "AGE" => "",
                "TEAM_GENDER" => $team_gender,
                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                "PROVIDER_CLUB_ID" => $surrogateClubId
            ));
            $result_team = $SportsOne->importTeam($team_temp);
            print_r($result_team);
            echo "<br>Team angelegt!<br><br>";
            
            /**
             * ****************************************************************
             * Spieler
             * ****************************************************************
             */
            echo "Spieler:<br>";
            // Spieler der Mannschaft laden
            $spieler = HochwarthIT_AILGate::getMannschaftDao()->getLinkedSpieler($mannschaften[$j]['GGUID']['value'], '*');
            for ($k = 0; $k < sizeOf($spieler); $k ++) {
                // Eindeutige ID f�r Teammember aus MD5GGUID(Mannschaft)-SAISON(Mannschaft)-GGUID(Spieler) mit md5
                $spieler[$k]['MD5TEAMMEMBER']['value'] = md5($mannschaften[$j]['MD5GGUID']['value'] . '-' . $mannschaften[$j]['SAISON']['value'] . '-' . $spieler[$k]['GGUID']['value']);
                echo $k . ": " . $spieler[$k]['NAME']['value'] . ", " . $spieler[$k]['CHRISTIANNAME']['value'] . " (" . $spieler[$k]['GGUID']['value'] . ") SURROGATE_TEAM_MEMBER_ID: " . $spieler[$k]['MD5TEAMMEMBER']['value'] . "<br>";
                
                // Geburtstag
                if ($spieler[$k]['BIRTHDAY']['value'])
                    $birthday = date("Y-m-d", strtotime($spieler[$k]['BIRTHDAY']['value']));
                else
                    $birthday = null;
                
                //Geschlecht
                if ($spieler[$k]['GWGENDER']['value']==utf8_encode('m�nnlich'))
                {
                    $gender = 'male';
                    $salutation = 'M';
                }
                else if ($spieler[$k]['GWGENDER']['value']=='weiblich')
                {
                    $gender = 'female';
                    $salutation = 'F';
                }
                else {
                    $gender = '';
                    $salutation = '';
                }
                echo $gender." ".$salutation."<br>";
                             
                // Spieler in SportsOne anlegen
                $person_data = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                    "FIRST_NAME" => $spieler[$k]['CHRISTIANNAME']['value'],
                    "LAST_NAME" => $spieler[$k]['NAME']['value'],
                    "SALUTATION" => $salutation,
                    "GENDER" => $gender,
                    "BIRTHDAY" => $birthday,
                    //"HOME_COUTRY" => "DEU",
                    //"NATIONALITY" => "DEU",
                    "PROVIDER_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                    "PROVIDER_CLUB_ID" => $surrogateClubId
                ));
                echo "<br>" . $person_data . "<br>";
                $result_player = $SportsOne->importPerson($person_data);
                // print_r($result_player);
                
                // Adressdaten
                $address_data = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_ADDRESS_ID" => $spieler[$k]['GGUID']['value'], // SURROGATE_PERSON_ADDRESS_ID = GGUID
                    "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                    // TODO Mapping des Landes!
                    "COUNTRY" => "",
                    "POST_CODE" => $spieler[$k]['ZIP3']['value'],
                    "CITY" => $spieler[$k]['TOWN3']['value'],
                    "STREET" => $spieler[$k]['STREET3']['value']
                ));
                $result_player_address = $SportsOne->importPersonAddress($address_data);
                // print_r($result_player_address);
                
                // Kontaktdaten
                $phone_data1 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("TelefonG" . $spieler[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Phone",
                    "VALUE" => "Telefon gesch.",
                    "DISPLAY_VALUE" => $spieler[$k]['PHONEFIELDSTR4']['value']
                ));
                $result_player_phone1 = $SportsOne->importPersonCommunication($phone_data1);
                // print_r($result_player_phone1);
                $phone_data2 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("TelefonP" . $spieler[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Phone",
                    "VALUE" => "Telefon privat",
                    "DISPLAY_VALUE" => $spieler[$k]['PHONEFIELDSTR7']['value']
                ));
                $result_player_phone2 = $SportsOne->importPersonCommunication($phone_data2);
                // print_r($result_player_phone2);
                $phone_data3 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("TelefonM" . $spieler[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Phone",
                    "VALUE" => "Telefon mobil",
                    "DISPLAY_VALUE" => $spieler[$k]['PHONEFIELDSTR2']['value']
                ));
                $result_player_phone3 = $SportsOne->importPersonCommunication($phone_data3);
                // print_r($result_player_phone3);
                $phone_data4 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("EmailG" . $spieler[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Email",
                    "VALUE" => "Email gesch.",
                    "DISPLAY_VALUE" => $spieler[$k]['MAILFIELDSTR1']['value']
                ));
                $result_player_phone4 = $SportsOne->importPersonCommunication($phone_data4);
                // print_r($result_player_phone4);
                $phone_data5 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("EmailP" . $spieler[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Email",
                    "VALUE" => "Email privat",
                    "DISPLAY_VALUE" => $spieler[$k]['MAILFIELDSTR3']['value']
                ));
                $result_player_phone5 = $SportsOne->importPersonCommunication($phone_data5);
                // print_r($result_player_phone5);
                echo "<br>Spieler angelegt!<br><br>";
                
                
                
                
                
                //�berpr�fung ob neue Mannschaft oder gleiche
                if (!empty($spieler[$k]['N_CURTEAMMEMBER']['value']))
                {
                    if($spieler[$k]['MD5TEAMMEMBER']['value']==$spieler[$k]['N_CURTEAMMEMBER']['value'])  {
                        //Gleiche Mannschaft
                        if(date("Y-m-d") > $spieler[$k]['N_CURTEAMMEMBER_END']['value']) {
                            $teammember_data = json_encode(array(
                                "PROVIDER_ID" => "CAS",
                                "SURROGATE_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                                "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                                "PERSON_ROLE" => "Player",
                                "POSITION" => "NOPOS",
                                "START_DATE" => $spieler[$k]['N_CURTEAMMEMBER_START']['value'],
                                "END_DATE" => $saison_ende,
                                "PROVIDER_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "PROVIDER_CLUB_ID" => $surrogateClubId
                            ));
                            $result_teammember = $SportsOne->importTeamMember($teammember_data);
                            print_r($result_teammember);
                            
                            // Aktuelle SURROGATE_TEAMMEMBER_ID in gW speichern
                            $aktgw_spieler = HochwarthIT_AILGate::getAdressDao()->load($spieler[$k]['GGUID']['value']);
                            //$aktgw_spieler->setValue($spieler[$k]['MD5TEAMMEMBER']['value'], 'N_CURTEAMMEMBER', 'STRING');
                            //$aktgw_spieler->setValue($saison_start, 'N_CURTEAMMEMBER_START', 'DATETIME');
                            $aktgw_spieler->setValue($saison_ende, 'N_CURTEAMMEMBER_END', 'DATETIME');
                            $aktgw_spieler = HochwarthIT_AILGate::getAdressDao()->save($aktgw_spieler);
                            echo "<br>Enddatum angepasst $saison_ende ".$aktgw_spieler->getValue('N_CURTEAMMEMBER')."<br><br>";
                        }
                        else {
                            //Keine �nderung erforderlich
                            echo "Keine �nderung! <br><br>";
                        }
                    }
                    else {
                        //Andere Mannschaft
                        if(date("Y-m-d") > $spieler[$k]['N_CURTEAMMEMBER_END']['value']) {
                            $teammember_data = json_encode(array(
                                "PROVIDER_ID" => "CAS",
                                "SURROGATE_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                                "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                                "PERSON_ROLE" => "Player",
                                "POSITION" => "NOPOS",
                                "START_DATE" => $saison_start,
                                "END_DATE" => $saison_ende,
                                "PROVIDER_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "PROVIDER_CLUB_ID" => $surrogateClubId
                            ));
                            $result_teammember = $SportsOne->importTeamMember($teammember_data);
                            print_r($result_teammember);
                            
                            // Aktuelle SURROGATE_TEAMMEMBER_ID in gW speichern
                            $aktgw_spieler = HochwarthIT_AILGate::getAdressDao()->load($spieler[$k]['GGUID']['value']);
                            $aktgw_spieler->setValue($spieler[$k]['MD5TEAMMEMBER']['value'], 'N_CURTEAMMEMBER', 'STRING');
                            $aktgw_spieler->setValue($saison_start, 'N_CURTEAMMEMBER_START', 'DATETIME');
                            $aktgw_spieler->setValue($saison_ende, 'N_CURTEAMMEMBER_END', 'DATETIME');
                            $aktgw_spieler = HochwarthIT_AILGate::getAdressDao()->save($aktgw_spieler);
                            echo "<br>Spieler verkn�pft! " . $aktgw_spieler->getValue('N_CURTEAMMEMBER') . "<br><br>";
                            echo "Regul�re Neuzuordnung $saison_start bis $saison_ende <br><br>";
                        }
                        else {
                            $teammember_data = json_encode(array(
                                "PROVIDER_ID" => "CAS",
                                "SURROGATE_TEAM_MEMBER_ID" => $spieler[$k]['N_CURTEAMMEMBER']['value'],
                                //"SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                                "PERSON_ROLE" => "Player",
                                "POSITION" => "NOPOS",
                                "START_DATE" => $spieler[$k]['N_CURTEAMMEMBER_START']['value'],
                                "END_DATE" => $gestern,
                                "PROVIDER_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "PROVIDER_CLUB_ID" => $surrogateClubId
                            ));
                            $result_teammember = $SportsOne->importTeamMember($teammember_data);
                            print_r($result_teammember);
                            echo "<br>�nderung alte Zuordnung Ende $gestern <br><br>";
                            
                            $teammember_data = json_encode(array(
                                "PROVIDER_ID" => "CAS",
                                "SURROGATE_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                                "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                                "PERSON_ROLE" => "Player",
                                "POSITION" => "NOPOS",
                                "START_DATE" => $heute,
                                "END_DATE" => $saison_ende,
                                "PROVIDER_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "PROVIDER_CLUB_ID" => $surrogateClubId
                            ));
                            $result_teammember = $SportsOne->importTeamMember($teammember_data);
                            print_r($result_teammember);
                            
                            // Aktuelle SURROGATE_TEAMMEMBER_ID in gW speichern
                            $aktgw_spieler = HochwarthIT_AILGate::getAdressDao()->load($spieler[$k]['GGUID']['value']);
                            $aktgw_spieler->setValue($spieler[$k]['MD5TEAMMEMBER']['value'], 'N_CURTEAMMEMBER', 'STRING');
                            $aktgw_spieler->setValue($heute, 'N_CURTEAMMEMBER_START', 'DATETIME');
                            $aktgw_spieler->setValue($saison_ende, 'N_CURTEAMMEMBER_END', 'DATETIME');
                            $aktgw_spieler = HochwarthIT_AILGate::getAdressDao()->save($aktgw_spieler);
                            echo "<br>Spieler verkn�pft! " . $aktgw_spieler->getValue('N_CURTEAMMEMBER') . "<br><br>";
                            echo "Neuzuordnung $heute bis $saison_ende <br><br>";                            
                        }   
                    }  
                }
                else {
                    // Neue Mannschaft ohne vorherige Verkn�pfung
                    $teammember_data = json_encode(array(
                        "PROVIDER_ID" => "CAS",
                        "SURROGATE_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                        "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                        "SURROGATE_PERSON_ID" => $spieler[$k]['GGUID']['value'],
                        "PERSON_ROLE" => "Player",
                        "POSITION" => "NOPOS",
                        "START_DATE" => $saison_start,
                        "END_DATE" => $saison_ende,
                        "PROVIDER_TEAM_MEMBER_ID" => $spieler[$k]['MD5TEAMMEMBER']['value'],
                        "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                        "PROVIDER_CLUB_ID" => $surrogateClubId
                    ));
                    $result_teammember = $SportsOne->importTeamMember($teammember_data);
                    print_r($result_teammember);
                    
                    // Aktuelle SURROGATE_TEAMMEMBER_ID in gW speichern
                    $aktgw_spieler = HochwarthIT_AILGate::getAdressDao()->load($spieler[$k]['GGUID']['value']);
                    $aktgw_spieler->setValue($spieler[$k]['MD5TEAMMEMBER']['value'], 'N_CURTEAMMEMBER', 'STRING');
                    $aktgw_spieler->setValue($saison_start, 'N_CURTEAMMEMBER_START', 'DATETIME');
                    $aktgw_spieler->setValue($saison_ende, 'N_CURTEAMMEMBER_END', 'DATETIME');
                    $aktgw_spieler = HochwarthIT_AILGate::getAdressDao()->save($aktgw_spieler);
                    echo "<br>Spieler verkn�pft! " . $aktgw_spieler->getValue('N_CURTEAMMEMBER') . "<br><br>";
                }
            }
            
            /**
             * ****************************************************************
             * Trainer
             * ****************************************************************
             */
            echo "Trainer:<br>";
            
            // Trainer der Mannschaft laden
            $trainer = HochwarthIT_AILGate::getMannschaftDao()->getLinkedTrainer($mannschaften[$j]['GGUID']['value'], '*');
            for ($k = 0; $k < sizeOf($trainer); $k ++) {
                
                // Eindeutige ID f�r Teammember aus MD5GGUID(Mannschaft)-SAISON(Mannschaft)-GGUID(Trainer)
                $trainer[$k]['MD5TEAMMEMBER']['value'] = md5($mannschaften[$j]['MD5GGUID']['value'] . '-' . $mannschaften[$j]['SAISON']['value'] . '-' . $trainer[$k]['GGUID']['value']);
                echo $k . ": " . $trainer[$k]['NAME']['value'] . ", " . $trainer[$k]['CHRISTIANNAME']['value'] . " (" . $trainer[$k]['GGUID']['value'] . ") SURROGATE_TEAM_MEMBER_ID: " . $trainer[$k]['MD5TEAMMEMBER']['value'] . "<br>";
                if ($trainer[$k]['BIRTHDAY']['value'])
                    $birthday = date("Y-m-d", strtotime($trainer[$k]['BIRTHDAY']['value']));
                else
                    $birthday = null;
                    
                // Geschlecht
                if ($trainer[$k]['GWGENDER']['value'] == utf8_encode('m�nnlich')) 
                {
                    $gender = 'male';
                    $salutation = 'M';
                } else if ($trainer[$k]['GWGENDER']['value'] == 'weiblich') 
                {
                    $gender = 'female';
                    $salutation = 'F';
                } else 
                {
                    $gender = '';
                    $salutation = '';
                }
                echo $gender." ".$salutation."<br>";
                
                // Trainer in SportsOne anlegen
                $person_data = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                    "FIRST_NAME" => $trainer[$k]['CHRISTIANNAME']['value'],
                    "LAST_NAME" => $trainer[$k]['NAME']['value'],
                    "SALUTATION" => $salutation,
                    "GENDER" => $gender,
                    "BIRTHDAY" => $birthday,
                    //"HOME_COUTRY" => "DEU",
                    //"NATIONALITY" => "DEU",
                    "PROVIDER_PERSON_ID" => $trainer[$k]['GGUID']['value']
                ));
                $result_staff = $SportsOne->importPerson($person_data);
                // print_r($result_staff);
                
                // Adressdaten
                $address_data = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_ADDRESS_ID" => $trainer[$k]['GGUID']['value'], // SURROGATE_PERSON_ADDRESS_ID = GGUID
                    "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                    // TODO Mapping des Landes!
                    "COUNTRY" => "",
                    "POST_CODE" => $trainer[$k]['ZIP1']['value'],
                    "CITY" => $trainer[$k]['TOWN1']['value'],
                    "STREET" => $trainer[$k]['STREET1']['value']
                ));
                $result_player_address = $SportsOne->importPersonAddress($address_data);
                // print_r($result_player_address);
                
                // Kontaktdaten
                $phone_data1 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("TelefonG" . $trainer[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Phone",
                    "VALUE" => "Telefon gesch.",
                    "DISPLAY_VALUE" => $trainer[$k]['PHONEFIELDSTR4']['value']
                ));
                $result_player_phone1 = $SportsOne->importPersonCommunication($phone_data1);
                // print_r($result_player_phone1);
                $phone_data2 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("TelefonP" . $trainer[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Phone",
                    "VALUE" => "Telefon privat",
                    "DISPLAY_VALUE" => $trainer[$k]['PHONEFIELDSTR7']['value']
                ));
                $result_player_phone2 = $SportsOne->importPersonCommunication($phone_data2);
                // print_r($result_player_phone2);
                $phone_data3 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("TelefonM" . $trainer[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Phone",
                    "VALUE" => "Telefon mobil",
                    "DISPLAY_VALUE" => $trainer[$k]['PHONEFIELDSTR2']['value']
                ));
                $result_player_phone3 = $SportsOne->importPersonCommunication($phone_data3);
                // print_r($result_player_phone3);
                $phone_data4 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("EmailG" . $trainer[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Email",
                    "VALUE" => "Email gesch.",
                    "DISPLAY_VALUE" => $trainer[$k]['MAILFIELDSTR1']['value']
                ));
                $result_player_phone4 = $SportsOne->importPersonCommunication($phone_data4);
                // print_r($result_player_phone4);
                $phone_data5 = json_encode(array(
                    "PROVIDER_ID" => "CAS",
                    "SURROGATE_PERSON_COMMUNICATION_ID" => md5("EmailP" . $trainer[$k]['GGUID']['value']), // SURROGATE_PERSON_COMMUNICATION_ID = md5("ART".GGUID)
                    "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                    "COMMUNICATION_TYPE" => "Email",
                    "VALUE" => "Email privat",
                    "DISPLAY_VALUE" => $trainer[$k]['MAILFIELDSTR3']['value']
                ));
                $result_player_phone5 = $SportsOne->importPersonCommunication($phone_data5);
                // print_r($result_player_phone5);
                echo "<br>Trainer angelegt!<br><br>";
                
                
                
                //�berpr�fung ob neue Mannschaft oder gleiche
                if (!empty($trainer[$k]['N_CURTEAMMEMBER']['value']))
                {
                    if($trainer[$k]['MD5TEAMMEMBER']['value']==$trainer[$k]['N_CURTEAMMEMBER']['value'])  {
                        //Gleiche Mannschaft
                        if(date("Y-m-d") > $trainer[$k]['N_CURTEAMMEMBER_END']['value']) {
                            $teammember_data = json_encode(array(
                                "PROVIDER_ID" => "CAS",
                                "SURROGATE_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                                "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                                "PERSON_ROLE" => "Staff",
                                "POSITION" => "TR",
                                "START_DATE" => $trainer[$k]['N_CURTEAMMEMBER_START']['value'],
                                "END_DATE" => $saison_ende,
                                "PROVIDER_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "PROVIDER_CLUB_ID" => $surrogateClubId
                            ));
                            $result_teammember = $SportsOne->importTeamMember($teammember_data);
                            print_r($result_teammember);
                            
                            // Aktuelle SURROGATE_TEAMMEMBER_ID in gW speichern
                            $aktgw_trainer = HochwarthIT_AILGate::getAdressDao()->load($trainer[$k]['GGUID']['value']);
                            //$aktgw_trainer->setValue($trainer[$k]['MD5TEAMMEMBER']['value'], 'N_CURTEAMMEMBER', 'STRING');
                            //$aktgw_trainer->setValue($saison_start, 'N_CURTEAMMEMBER_START', 'DATETIME');
                            $aktgw_trainer->setValue($saison_ende, 'N_CURTEAMMEMBER_END', 'DATETIME');
                            $aktgw_trainer = HochwarthIT_AILGate::getAdressDao()->save($aktgw_trainer);
                            echo "<br>Enddatum angepasst $saison_ende ".$aktgw_trainer->getValue('N_CURTEAMMEMBER')."<br><br>";
                        }
                        else {
                            //Keine �nderung erforderlich
                            echo "Keine �nderung! <br><br>";
                        }
                    }
                    else {
                        //Andere Mannschaft
                        if(date("Y-m-d") > $trainer[$k]['N_CURTEAMMEMBER_END']['value']) {
                            $teammember_data = json_encode(array(
                                "PROVIDER_ID" => "CAS",
                                "SURROGATE_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                                "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                                "PERSON_ROLE" => "Staff",
                                "POSITION" => "TR",
                                "START_DATE" => $saison_start,
                                "END_DATE" => $saison_ende,
                                "PROVIDER_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "PROVIDER_CLUB_ID" => $surrogateClubId
                            ));
                            $result_teammember = $SportsOne->importTeamMember($teammember_data);
                            print_r($result_teammember);
                            
                            // Aktuelle SURROGATE_TEAMMEMBER_ID in gW speichern
                            $aktgw_trainer = HochwarthIT_AILGate::getAdressDao()->load($trainer[$k]['GGUID']['value']);
                            $aktgw_trainer->setValue($trainer[$k]['MD5TEAMMEMBER']['value'], 'N_CURTEAMMEMBER', 'STRING');
                            $aktgw_trainer->setValue($saison_start, 'N_CURTEAMMEMBER_START', 'DATETIME');
                            $aktgw_trainer->setValue($saison_ende, 'N_CURTEAMMEMBER_END', 'DATETIME');
                            $aktgw_trainer = HochwarthIT_AILGate::getAdressDao()->save($aktgw_trainer);
                            echo "<br>Trainer verkn�pft! " . $aktgw_trainer->getValue('N_CURTEAMMEMBER') . "<br><br>";
                            echo "Regul�re Neuzuordnung $saison_start bis $saison_ende <br><br>";
                        }
                        else {
                            $teammember_data = json_encode(array(
                                "PROVIDER_ID" => "CAS",
                                "SURROGATE_TEAM_MEMBER_ID" => $trainer[$k]['N_CURTEAMMEMBER']['value'],
                                //"SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                                "PERSON_ROLE" => "Staff",
                                "POSITION" => "TR",
                                "START_DATE" => $trainer[$k]['N_CURTEAMMEMBER_START']['value'],
                                "END_DATE" => $gestern,
                                "PROVIDER_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "PROVIDER_CLUB_ID" => $surrogateClubId
                            ));
                            $result_teammember = $SportsOne->importTeamMember($teammember_data);
                            print_r($result_teammember);
                            echo "<br>�nderung alte Zuordnung Ende $gestern <br><br>";
                            
                            $teammember_data = json_encode(array(
                                "PROVIDER_ID" => "CAS",
                                "SURROGATE_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                                "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                                "PERSON_ROLE" => "Staff",
                                "POSITION" => "TR",
                                "START_DATE" => $heute,
                                "END_DATE" => $saison_ende,
                                "PROVIDER_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                                "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                                "PROVIDER_CLUB_ID" => $surrogateClubId
                            ));
                            $result_teammember = $SportsOne->importTeamMember($teammember_data);
                            print_r($result_teammember);
                            
                            // Aktuelle SURROGATE_TEAMMEMBER_ID in gW speichern
                            $aktgw_trainer = HochwarthIT_AILGate::getAdressDao()->load($trainer[$k]['GGUID']['value']);
                            $aktgw_trainer->setValue($trainer[$k]['MD5TEAMMEMBER']['value'], 'N_CURTEAMMEMBER', 'STRING');
                            $aktgw_trainer->setValue($heute, 'N_CURTEAMMEMBER_START', 'DATETIME');
                            $aktgw_trainer->setValue($saison_ende, 'N_CURTEAMMEMBER_END', 'DATETIME');
                            $aktgw_trainer = HochwarthIT_AILGate::getAdressDao()->save($aktgw_trainer);
                            echo "<br>Trainer verkn�pft! " . $aktgw_trainer->getValue('N_CURTEAMMEMBER') . "<br><br>";
                            echo "Neuzuordnung $heute bis $saison_ende <br><br>";
                        }
                    }
                }
                else {
                    // Neue Mannschaft ohne vorherige Verkn�pfung
                    $teammember_data = json_encode(array(
                        "PROVIDER_ID" => "CAS",
                        "SURROGATE_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                        "SURROGATE_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                        "SURROGATE_PERSON_ID" => $trainer[$k]['GGUID']['value'],
                        "PERSON_ROLE" => "Staff",
                        "POSITION" => "TR",
                        "START_DATE" => $saison_start,
                        "END_DATE" => $saison_ende,
                        "PROVIDER_TEAM_MEMBER_ID" => $trainer[$k]['MD5TEAMMEMBER']['value'],
                        "PROVIDER_TEAM_ID" => $mannschaften[$j]['MD5GGUID']['value'],
                        "PROVIDER_CLUB_ID" => $surrogateClubId
                    ));
                    $result_teammember = $SportsOne->importTeamMember($teammember_data);
                    print_r($result_teammember);
                    
                    // Aktuelle SURROGATE_TEAMMEMBER_ID in gW speichern
                    $aktgw_trainer = HochwarthIT_AILGate::getAdressDao()->load($trainer[$k]['GGUID']['value']);
                    $aktgw_trainer->setValue($trainer[$k]['MD5TEAMMEMBER']['value'], 'N_CURTEAMMEMBER', 'STRING');
                    $aktgw_trainer->setValue($saison_start, 'N_CURTEAMMEMBER_START', 'DATETIME');
                    $aktgw_trainer->setValue($saison_ende, 'N_CURTEAMMEMBER_END', 'DATETIME');
                    $aktgw_trainer = HochwarthIT_AILGate::getAdressDao()->save($aktgw_trainer);
                    echo "<br>Trainer verkn�pft! " . $aktgw_trainer->getValue('N_CURTEAMMEMBER') . "<br><br>";
                }
            }
            
            echo "<br>";
        }
        echo "<br><br>";
    }
}
echo "</pre> Ende";
?>

