<?php
class SportsOneApi {
    //LIVE-SERVER
    private $host   = 'https://sapsportsonea67310256.hana.ondemand.com';
    private $api_key = '';
    private $token  = '';
    private $secret  = '';
    private $sso_url = '';

	 /**
     * Example1 hooks into the pages->save method and displays a notice every time a page is saved
     * @param null $service_url
     * @param null $userData
     * @param null $httpVerb
     * @return mixed
     * @internal param null $user
     */
	public function sendRequest($service_url, $header, $data=Array(), $httpVerb = 'post') {

	    
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_VERBOSE, true);
	    curl_setopt($ch, CURLOPT_URL, $service_url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    
	    if($httpVerb == 'GET'){
	        curl_setopt($ch, CURLOPT_HTTPGET, true);
	    }
	    
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_HEADER , 1);
	    
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    
	    //todo data
	    if(count($data) > 0){
	        
	    }
	    
	    $response = curl_exec($ch);
	    
	    curl_close($ch);


        return $response;
	}
	
	static function getToken($login){
	    
	    $service_url = "https://sapsportsonea67310256.hana.ondemand.com/sap/sports/fnd/db/services/public/xs/token.xsjs";
// 	    $service_url = "https://www.google.de";
	   
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_VERBOSE, true);
	    curl_setopt($ch, CURLOPT_URL, $service_url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPGET, true);
	    
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    
	    curl_setopt($ch, CURLOPT_HEADER , 1);
	    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
	    $header = Array(
	        "Content-Type: application/json",
	        "Accept: application/json",
	        "X-CSRF-Token: Fetch",
	        "Cookie: ".$login
	        );
	  
	   
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    
	    // Send the request & save response to $resp
	    $response = curl_exec($ch);
	    echo "<pre>";
	    $info = curl_getinfo($ch);
	    //var_dump($info);
	    //print_r($response);
	
		//echo('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
	    #$http_status = curl_getinfo($ch);
	    #print_r($http_status);
	    
	    
	    $start = strpos($response, 'x-csrf-token:');
	    $token = substr($response, $start+14, 32);
	    echo "Token: ".$token."<br><br>";
	    
	    // Close request to clear up some resources
	    curl_close($ch);
	    	  
	    return $token;
	    
	}
	static function getLogin(){
	    $service_url = "https://sapsportsonea67310256.hana.ondemand.com/sap/hana/xs/formLogin/login.xscfunc";
	    
	    $ch = curl_init();
	    //curl_setopt($ch, CURLOPT_VERBOSE, true);
	    curl_setopt($ch, CURLOPT_URL, $service_url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    //curl_setopt($ch, CURLOPT_HTTPGET, true);
	    //curl_setopt($ch, CURLOPT_HTTPPOST, true);
	    
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	        "x-csrf-token: unsafe"
	    ));
	    curl_setopt($ch, CURLOPT_HEADER  ,1);
	    
	    $userData = 'xs-username=BAUMANNR&xs-password=AiL128!!';
	    curl_setopt($ch, CURLOPT_POSTFIELDS,  $userData);
	
	    
	    // Send the request & save response to $resp
	    $response = curl_exec($ch);
	   
	    
	    $cookies = array();
	    preg_match_all('/set-cookie:(?<cookie>\s{0,}.*)$/im', $response, $cookies);

	    $result = str_replace(' ','', $cookies['cookie'][0]).';'.str_replace(' ','', $cookies['cookie'][1]);
	    $result = str_replace("\n", '', $result);
	    $result = str_replace("\r", '', $result);
	    
	    //echo('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch).'<br><br>');
	    
	    //print_r($response);
	    
	    #echo "Login erfolgreich! Cookie: ".$result;
	            
	    // Close request to clear up some resources
	    curl_close($ch);
	    return $result;    
	}
	
	static function importPerson($token, $login){
	    $service_url = "https://sapsportsonea67310256.hana.ondemand.com/sap/sports/imp/api/copyQueue/service/rest/copyQueue/PERSON/v1";
	    
	    $ch = curl_init();
	    //curl_setopt($ch, CURLOPT_VERBOSE, true);
	    curl_setopt($ch, CURLOPT_URL, $service_url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    
	    $header = array(
	        "Content-Type: application/json",
 	        "Authorization: Basic Og==",
	        "X-CSRF-Token: ".$token,
	        "Cookie: ".$login
	        
	    );
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_HEADER , 1);
	    
	    $data = json_encode(
	        array("PROVIDER_ID"=>"CAS",
	              "SURROGATE_PERSON_ID"=>"12345678901234567890123456789123",
	              "FIRST_NAME"=>"Test",
	              "LAST_NAME"=>"Testmann",
	              "SALUTATION"=>"M",
	              "GENDER"=>"male",
	              "BIRTHDAY"=>"2000-01-01",
	              "HOME_COUTRY"=>"DEU",
	              "NATIONALITY"=>"DEU",
	              "PROVIDER_PERSON_ID"=>"Testmann789"));
	    curl_setopt($ch, CURLOPT_POSTFIELDS,  $data);
	     
	    
	   
	    print_r($header);  echo "<br>";echo "<br>";
	    print_r($data);    echo "<br>";echo "<br>";
	    	    
	    // Send the request & save response to $resp
	    $response = curl_exec($ch);  
	    print_r($response);
	    	  
	    // Close request to clear up some resources
	    curl_close($ch);
	    return $response;
	}

}