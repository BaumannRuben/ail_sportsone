<?php
set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ .'/Classes');
require_once 'de.cas.gw.php.api/ClassLoader.php';

use \de\cas\open\server\api\IEIMInterface;
use \de\cas\open\server\api\eiminterface\EIMWebServiceInterface;
use \de\cas\open\server\api\eiminterface\EIMInterfaceFactory;
use \de\cas\open\server\api\eiminterface\security\ISecurityToken;
use \de\cas\open\server\api\eiminterface\security\ISecurityTokenProvider;
use \de\cas\open\server\api\eiminterface\security\UsernamePasswordToken;
use \de\cas\open\server\api\types\DataObject;
use \de\cas\open\server\api\types\DataObjectDescription;
use \de\cas\open\server\api\types\FieldDescription;
use \de\cas\open\server\api\types\MassQueryResult;
use \de\cas\open\server\api\exceptions\DataLayerException;
use \de\cas\gw\server\core\types\LoginRequest;
use \de\cas\gw\server\core\types\LoginResponse;


class SecurityTokenProvider implements ISecurityTokenProvider
{

    static private $instance = null;

    protected $sessionTicket = '';
    protected $eimInterface = '';

    static public function getInstance()
      {
         if (null === self::$instance) {
             self::$instance = new self;
         }
         return self::$instance;
     }

    private function __construct(){
        $this->generateSessionTicket();
    }

    public function getSessionTicket()
    {
        return $this->sessionTicket;
    }

    private function setSessionTicket($sessionTicket)
    {
        $this->sessionTicket = $sessionTicket;
    }

    private function setEIMInterface($eimInterface)
    {
        $this->eimInterface = $eimInterface;
    }

    public function getEIMInterface()
    {
        return $this->eimInterface;
    }

    public function getSecurityToken($soapAction)
    {
        if (!empty($this->sessionTicket)) {
            return new UsernamePasswordToken($this->sessionTicket, '');
        } else {
            return NULL;
        }
    }

    private function generateSessionTicket()
    {

        $credentialRewriteFile = dirname(__FILE__)."/SecurityTokenLive.php";

        $serviceLocation = 'http://192.168.25.8:8080/genesis.svc';
        $productKey = 'AnpassungUndDatenmigration_SOAP+REST_EE14_keHAU++XV/wyhr7yW+kBXvhmZ6UEHevM8ZtyG4vSnhCUZ8C5h07zTL12n0VN4d3igiVDmbaKPE7yUuBLtcuPcg==';
        $database = "ANPFIFFINSLEBEN_EV";
        $user = utf8_encode("Dietmar Pf�hler");
        $password = "";

        if(file_exists($credentialRewriteFile)){
            include $credentialRewriteFile;
        }


        $eimInterfaceFactory = new EIMInterfaceFactory($this, $serviceLocation, $productKey);
        $eimInterface = $eimInterfaceFactory->getEIMInterface();
        $loginRequest = new LoginRequest();
        $loginRequest->Database = $database;
        $loginRequest->Username = $user;
        $loginRequest->Password = $password;

        $loginResponse = $eimInterface->executeUnauthenticated($loginRequest);
        $sessionTicket = $loginResponse->SessionTicket;
        $this->setSessionTicket($sessionTicket);
        $this->setEIMInterface($eimInterface);

    }
}