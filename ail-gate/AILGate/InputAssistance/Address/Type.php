<?php

class HochwarthIT_AILGate_InputAssistance_Address_Type{
    const ADDRESS_TYPE_COMP = 1;
    const ADDRESS_TYPE_SHIPPING = 2;
    const ADDRESS_TYPE_PRIVATE = 3;

    static function getAllOptions(){
        return array(
            array("value" => self::ADDRESS_TYPE_COMP, "label" => "Firmenadresse"),
            array("value" => self::ADDRESS_TYPE_SHIPPING, "label" => "Lieferadresse"),
            array("value" => self::ADDRESS_TYPE_PRIVATE, "label" => "Privatadresse")
        );
    }

    static function getCasAddressTypes(){
        return self::getAllOptions();
    }

}