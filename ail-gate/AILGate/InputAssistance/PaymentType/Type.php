<?php

class HochwarthIT_AILGate_InputAssistance_PaymentType_Type{

    public static $paymentTypes = array(
        "checkmo" => "[Z1]",
        "debit" => "[Z2]",
        "bankpayment" => "[Z3]",
        "phoenix_cashondelivery" => "[Z4]",
        "paypal_standard" => "[Z6]"
    );

}