<?php

class HochwarthIT_AILGate_InputAssistance_Event_Status{
    const EVENT_STATUS_OFFEN = "offen";
    const EVENT_STATUS_BESTAETIGT = "bestätigt";
    const EVENT_STATUS_WARTELISTE = "Warteliste";
    const EVENT_STATUS_ABGELEHNT = "abgelehnt";
    const EVENT_STATUS_STORNIERT = "storniert";
    const EVENT_STATUS_STORNIERT_BESTAETIGT = "storniert bestätigt";

    static function getAllOptions(){
        return array(
            array("value" => 1, "label" => self::EVENT_STATUS_OFFEN),
            array("value" => 2, "label" => self::EVENT_STATUS_BESTAETIGT),
            array("value" => 3, "label" => self::EVENT_STATUS_WARTELISTE),
            array("value" => 4, "label" => self::EVENT_STATUS_ABGELEHNT),
            array("value" => 5, "label" => self::EVENT_STATUS_STORNIERT),
            array("value" => 6, "label" => self::EVENT_STATUS_STORNIERT_BESTAETIGT),
        );
    }

}