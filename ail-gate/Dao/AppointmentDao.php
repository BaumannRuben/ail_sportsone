<?php

require_once "AbstractDao.php";

class AppointmentDao extends AbstractDao
{

    protected $objectType = "APPOINTMENT";
    static private $instance = null;

    /* @return AppointmentDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /**
     * Get all Registrations for a specific Appointment
     *
     * @param $app_gguid String Appointment GGUID
     * @return array
     */
    public function getRegistrations($app_gguid, $address_gguid = null){
        $sql = "SELECT GGUID, REG_EVENTGUID, REG_KEYWORD, REG_ADDGUID, REG_ADDSHORTINFO, REG_REGSHORTINFO, REG_COMPANION, REG_PAYSHORTINFO, REG_TYPE, REG_PARTICIPANCE"
            . " FROM REGISTRATION AS R"
            . " WHERE REG_EVENTGUID = '0x$app_gguid'";

        if($address_gguid){
            $sql .= " AND REG_ADDGUID = '0x$address_gguid'";
        }

        $sql = self::addTeamfilterToSql($sql, "REGISTRATION");

        $result = SecurityTokenProvider::getInstance()->getEIMInterface()->query($sql);
        return $this->convertResultObject($result);

    }

    /**
     * Get all Appointments for a specific Appointment
     *
     * @param $app_gguid String Appointment GGUID
     * @return array
     */
    public function getAppointmentsOfEvent($ev_gguid){
        $sql = "SELECT *"
            . " FROM APPOINTMENT AS A"
            . " WHERE A.IsLinkedToWhere(EVENT: WHERE EVENT.GGUID = 0x$ev_gguid)";
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);

        $result = SecurityTokenProvider::getInstance()->getEIMInterface()->query($sql);
        return $this->convertResultObject($result);

    }

}