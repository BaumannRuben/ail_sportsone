<?php

require_once "AbstractDao.php";

use \de\cas\open\server\api\types\LinkObject;
use \de\cas\open\server\api\business\SaveLinkRequest;

class BillDao extends AbstractDao
{

    protected $objectType = "IFAA_BELEGE";
    static private $instance = null;

    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    public function createBill($billData)
    {
        $billObj = $this->createObject();
        foreach ($billData as $field => $row) {
            $billObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($billObj);
    }

    public function updateBill($billObj, $billData)
    {
        foreach ($billData as $field => $row) {
            $billObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($billObj);
    }

    public function createLink($billGguid, $customerGguid)
    {
        $link = new LinkObject();
        $link->objectType1 = "IFAA_BELEGE";
        $link->GGUID1 = $billGguid;
        $link->objectType2 = "ADDRESS";
        $link->GGUID2 = $customerGguid;
//        $link->attribute->key = "L2UADRHIERARCHY";
        $link->isHierarchy = true;
        $link->linkDirection = "RIGHT_TO_LEFT"; //Right = Tochter, Left = Mutter

        $linkRequest = new SaveLinkRequest();
        $linkRequest->links = array($link);

        $linkResponse = HochwarthIT_AILGate::getEIMInterface()->execute($linkRequest);

        return $linkResponse;
    }
}