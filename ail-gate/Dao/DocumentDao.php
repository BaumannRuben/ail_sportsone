<?php
require_once "AbstractDao.php";

use de\cas\open\server\documents\types\CheckInFileRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

use de\cas\open\server\documents\types\CheckOutFileRequest;

class DocumentDao extends AbstractDao
{
    protected $objectType = "DOCUMENT";
    static private $instance = null;

    /* @return DocumentDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /*
     *  Erstellt eine neues Dokument
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function createDocument($documentdata)
    {
    	$documentObj = $this->createObject();
        foreach ($documentdata as $field => $row) {
            $documentObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($documentObj);
    }

    /*
     *  Fuegt dem uebergebenen Objekt eine Datei hinzu
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function uploadDocument($dataObjectTrans, $filedata, $filetype)
    {
    	$docRequest = new CheckInFileRequest();
    	$docRequest->dataObjectToSave = $dataObjectTrans;
    	$docRequest->documentContent = $filedata;
    	$docRequest->fileType = $filetype;
    	$docResponse = HochwarthIT_AILGate::getEIMInterface()->execute($docRequest);
    	return $docResponse;
    }
    
    public function createLinkBetweenAdrDoc($adrgguid, $docgguid)
    {
    	
    	$link = new LinkObject();
    	$link->objectType1 = "ADDRESS";
    	$link->GGUID1 = $adrgguid;
    	$link->objectType2 = "DOCUMENT";
    	$link->GGUID2 = $docgguid;
    	$link->attribute = new StdClass();
    	$link->attribute->key = "ITDDOCADR";
    	$link->isHierarchy = true;
    
    	$linkRequest = new SaveLinkRequest();
    	$linkRequest->links = array($link);
    
    	$linkResponse = HochwarthIT_AILGate::getEIMInterface()->execute($linkRequest);
    
    	return $linkResponse;
    }
    
    //Download Dokument
    public function downloadDocument($doc_gguid) {
    	$docRequest = new CheckOutFileRequest();
    	$docRequest->GGUID = "0x".$doc_gguid;
    	$docRequest->readOnly=true;
      	$docResponse = HochwarthIT_AILGate::getEIMInterface()->execute($docRequest);
    	 
    	return $docResponse;
    }
}