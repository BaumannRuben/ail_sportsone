<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

//Upload Kontaktbild
use de\cas\open\server\addresses\types\SaveImageForContactRequest;
use de\cas\open\server\api\business\GetJournalRequest;
use de\cas\open\server\addresses\types\GetImageForContactRequest;

class TodoDao extends AbstractDao
{

    protected $objectType = "TODO";
    static private $instance = null;

    /* @return TodoDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /*
     *  Erstellt eine neue Aufgabe
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function createTodo($tododata)
    {
    	$todoObj = $this->createObject();
        foreach ($tododata as $field => $row) {
            $todoObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($todoObj);
    }
    
    public function createLinkBetweenStartrechtTodo($gguid1, $gguid2, $attribute)
    {
    	$link = new LinkObject();
    	$link->objectType1 = "STARTRECHT";
    	$link->GGUID1 = $gguid1;
    	$link->objectType2 = "TODO";
    	$link->GGUID2 = $gguid2;
    	$link->attribute = new StdClass();
    	$link->attribute->key = $attribute;
    	$link->isHierarchy = true;
    
    	$linkRequest = new SaveLinkRequest();
    	$linkRequest->links = array($link);
    
    	$linkResponse = HochwarthIT_AILGate::getEIMInterface()->execute($linkRequest);
    
    	return $linkResponse;
    }
}
