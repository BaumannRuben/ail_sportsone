<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Ends a geo session.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: DeleteGeoDataSessionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteGeoDataSessionRequest
     */
    class DeleteGeoDataSessionResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
