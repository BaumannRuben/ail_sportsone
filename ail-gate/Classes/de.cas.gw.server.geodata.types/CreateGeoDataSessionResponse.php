<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Starts a new geo session.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CreateGeoDataSessionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CreateGeoDataSessionRequest
     */
    class CreateGeoDataSessionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\gw\server\geodata\types\GeoDataSessionInfo
         *
         */
        public $Settings;

    }

}
