<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Retrieves the heatmap image bytes for a given tile.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetGeoDataContactsInAreaResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGeoDataContactsInAreaResponse
     */
    class GetGeoDataHeatmapResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *                    Sets/Returns the bytes of the image.
         */
        public $ImageBytes;

    }

}
