<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets the contacts in the circumcircle.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetGeoDataContactsInAreaResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetGeoDataContactsInAreaResponse
     */
    class GetGeoDataContactsInAreaRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\geodata\types\GeoDataSearchInputParameter
         *
         *                    Sets/Returns the GeoDataSearchInputParameter for the geodata request.
         */
        public $GeoDataSearchInputParameter;

        /**
         * @var string
         *
         *                    Sets/Returns the dataBase for the geodata request.
         */
        public $DataBase;

        /**
         * @var int
         *
         *                    Sets/Returns the geoSessionId for the geodata request.
         */
        public $GeoSessionId;

    }

}
