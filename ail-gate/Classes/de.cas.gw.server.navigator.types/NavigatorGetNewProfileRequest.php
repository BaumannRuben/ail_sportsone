<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Requests a empty navigator profile.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetNewProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetNewProfileResponse
     */
    class NavigatorGetNewProfileRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
