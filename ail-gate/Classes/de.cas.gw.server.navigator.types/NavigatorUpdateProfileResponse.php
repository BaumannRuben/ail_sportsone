<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the navigator profile for a given id.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetNavigatorProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetNavigatorProfileResponse
     */
    class NavigatorUpdateProfileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         */
        public $ProfileID;

    }

}
