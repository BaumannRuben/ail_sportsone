<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the navigator profile for a given id and datatype.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetProfileForDataTypeResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetProfileForDataTypeResponse
     */
    class NavigatorGetProfileForDataTypeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *The profile id
         */
        public $ProfileID;

        /**
         * @var string
         *The datatype
         */
        public $DataType;

        /**
         * @var array
         *The requested view kinds of the view definitions
         */
        public $ViewKinds;

    }

}
