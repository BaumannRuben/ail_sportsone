<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Sets the properties of a navigator node.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorSaveNodePropertiesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorSaveNodePropertiesResponse
     */
    class NavigatorSaveNodePropertiesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $ProfileID;

        /**
         * @var string
         *
         */
        public $NavNodeGUID;

        /**
         * @var array
         *
         */
        public $ParamList;

    }

}
