<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Deletes a new navigator profile from database.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorDeleteProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorDeleteProfileResponse
     */
    class NavigatorDeleteProfileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $ProfileID;

    }

}
