<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the navigator profile view for a given view id.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetNavigatorProfileViewRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetNavigatorProfileViewRequest
     */
    class NavigatorGetProfileViewResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *                    The navigator profile view for a given view id
         */
        public $ViewDefinition;

    }

}
