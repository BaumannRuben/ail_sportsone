<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets the ProfileID for the navigator start profile.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetStartProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetStartProfileResponse
     */
    class NavigatorGetStartProfileRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
