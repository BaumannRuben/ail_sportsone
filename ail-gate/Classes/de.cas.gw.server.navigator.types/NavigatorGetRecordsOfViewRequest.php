<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the records that are contained in a view of a navigator.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetRecordsOfViewResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetRecordsOfViewResponse
     */
    class NavigatorGetRecordsOfViewRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The profile id
         */
        public $ProfileID;

        /**
         * @var string
         *
         *                    The node id
         */
        public $NodeID;

        /**
         * @var string
         *
         *                    An additional WHERE-string
         */
        public $AdditionalWhereString;

        /**
         * @var int
         *
         *                    The first record
         */
        public $FirstRecord;

        /**
         * @var int
         *
         *                    The record count
         */
        public $RecordCount;

        /**
         * @var boolean
         *
         *                    Defines if all columns should be selected
         */
        public $SelectAllColumns;

        /**
         * @var array
         *
         *                    The columns to be selected
         */
        public $SelectColumns;

        /**
         * @var array
         *
         *                    The columns used to sort the result
         */
        public $OrderByColumns;

    }

}
