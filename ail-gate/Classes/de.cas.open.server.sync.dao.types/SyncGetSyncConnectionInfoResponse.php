<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\types {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\types
     *
     */
    class SyncGetSyncConnectionInfoResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncConnectionInfo
         *
         */
        public $syncConnectionInfo;

    }

}
