<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\types {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\types
     *
     */
    class SyncGetLinksFromLastTimeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $role;

        /**
         * @var array
         *
         */
        public $linksNow;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncSessionContext
         *
         */
        public $syncSessionContext;

    }

}
