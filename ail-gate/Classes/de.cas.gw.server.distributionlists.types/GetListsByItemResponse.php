<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Returns with the assiciated distribution lists for an address
     */
    class GetListsByItemResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										MassQueryResult with the following columns:
         *										- GGUID = GGUID
         *										- KEYWORD = Keyword of the distributor
         *										- DESCRIPTION = Description of the distributor
         */
        public $result;

    }

}
