<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Adds an address to the given distribution lists
     */
    class DeleteItemFromListsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										GGUIDs of the distribution lists
         */
        public $DistributionListGGUIDs;

        /**
         * @var string
         *
         *										GGUID of the address record
         */
        public $AddressGGUID;

    }

}
