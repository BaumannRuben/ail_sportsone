<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Deletes passed address (es) from the distribution list.
     *				See: https://partnerportal.cas.de/SDKDocu/#12765
     */
    class DeleteItemsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the distribution list record.
         */
        public $DistributionListGGUID;

        /**
         * @var array
         *
         *										GGUID of the address records.
         */
        public $AddressGGUIDs;

    }

}
