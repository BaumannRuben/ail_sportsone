<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a DecimalField.
     *	@see DecimalField
     */
    class DecimalFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var int
         *
         *										Sets/Returns the maximum precision of this field in the
         *										database. See {@link
         *										de.cas.eim.api.util.BigDecimalUtil#adjustScaleAndCheckPrecision(BigDecimal,
         *										int, int)}.
         */
        public $precision;

        /**
         * @var int
         *
         *										Sets/Returns the maximum scale of this field in the
         *										database. See {@link
         *										de.cas.eim.api.util.BigDecimalUtil#adjustScaleAndCheckPrecision(BigDecimal,
         *										int, int)}.
         */
        public $scale;

        /**
         * @var unknown
         *
         *                    Sets/Gets the minimum value of the DecimalField.
         */
        public $minValue;

        /**
         * @var unknown
         *
         *                    Sets/Gets the maximum value of the DecimalField.
         */
        public $maxValue;

    }

}
