<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\RequestObject:
     *		    It's the base type for all business operation
     *		    request objects. Corresponding ResponseObject: \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     */
    abstract class ManagementRequestObject {

    }

}
