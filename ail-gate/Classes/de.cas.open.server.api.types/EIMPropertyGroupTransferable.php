<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *		    An EIMPropertyGroupTransferable is a container that associates a property-group to
     *		    EIMPropertiesTransferable.
     */
    class EIMPropertyGroupTransferable {

        /**
         * @var string
         *The name of the property-group
         */
        public $name;

        /**
         * @var \de\cas\open\server\api\types\EIMPropertiesTransferable
         *
         *								The properties contained in this group.
         */
        public $properties;

    }

}
