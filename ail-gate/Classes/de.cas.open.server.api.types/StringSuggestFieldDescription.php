<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Describes a String Suggest Field.
     *        In a field of this type, only a single value can be selected from the suggested values.
     */
    class StringSuggestFieldDescription extends \de\cas\open\server\api\types\StringFieldDescription {

        /**
         * @var array
         *
         *                    The suggested values.
         */
        public $suggestedValues;

        /**
         * @var boolean
         *
         *                    Whether the user can freely edit the value of the field
         *                    or he is only allowed to choose from the predefined set of suggested values.
         */
        public $customValueAllowed;

    }

}
