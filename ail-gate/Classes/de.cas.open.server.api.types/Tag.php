<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Object representation of a tag.
     */
    class Tag {

        /**
         * @var string
         *Sets/Gets the GGUID of the tag
         */
        public $GGUID;

        /**
         * @var boolean
         *
         *								Is this tag system a system tag or not.
         */
        public $SystemTag;

        /**
         * @var boolean
         *Is this tag system defined or not.
         */
        public $SystemDefined;

        /**
         * @var boolean
         *
         *								Flag that indicates if the tag is editable through the user.
         */
        public $UserEditable;

        /**
         * @var int
         *
         *								Flag that indicates the sort position of a system tag.
         */
        public $SortIndex;

        /**
         * @var boolean
         *
         *								Flag that indicates if a system defined tag can be deleted by an administrator.
         *								These tags are not really deleted but flagged as being deleted, because they should not be
         *								re-inserted upon the next database update.
         */
        public $Deletable;

    }

}
