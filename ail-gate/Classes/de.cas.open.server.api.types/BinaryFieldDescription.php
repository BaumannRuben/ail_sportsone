<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a BinaryField.
     */
    class BinaryFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var int
         *
         *										Describes the maximum length of this field that can be
         *										stored in the database
         */
        public $maxLength;

    }

}
