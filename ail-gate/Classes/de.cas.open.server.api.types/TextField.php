<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a container for a TEXT value in a generic DataObject.
     */
    class TextField extends \de\cas\open\server\api\types\StringField {

    }

}
