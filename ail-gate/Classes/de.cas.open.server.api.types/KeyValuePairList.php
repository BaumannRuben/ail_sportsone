<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        List of KeyValuePairs.
     */
    class KeyValuePairList {

        /**
         * @var array
         *
         */
        public $list;

    }

}
