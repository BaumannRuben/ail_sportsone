<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a DateField
     */
    class DateFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var int
         *
         *										Sets/Returns the precision of this field in the
         *										database {see
         *										de.cas.eim.api.constants.DateFieldPrecision}.
         */
        public $precision;

    }

}
