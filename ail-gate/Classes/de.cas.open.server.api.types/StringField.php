<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for a String value in a generic DataObject
     */
    class StringField extends \de\cas\open\server\api\types\Field {

        /**
         * @var string
         *The String value of this field
         */
        public $value;

    }

}
