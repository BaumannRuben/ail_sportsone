<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Contains information about the external owners of a synced record.
     */
    class SyncRecordPermissions {

        /**
         * @var boolean
         *
         *                True if the current user is the organizer of the record.
         */
        public $isOrganizer;

        /**
         * @var boolean
         *
         *                True if the current user has the permission to edit the external owners of the record.
         */
        public $isParticipantEditingAllowed;

        /**
         * @var int
         *
         *                The the current users access right.
         */
        public $accessRight;

        /**
         * @var array
         *
         *                The external owners of the record.
         */
        public $externalOwners;

    }

}
