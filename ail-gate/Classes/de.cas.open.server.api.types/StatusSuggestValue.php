<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a status of a type.
     */
    class StatusSuggestValue {

        /**
         * @var string
         *
         *                The localized status value.
         */
        public $value;

        /**
         * @var array
         *
         *                The mandatory fields for records of this type/status combination.
         */
        public $mandatoryFields;

    }

}
