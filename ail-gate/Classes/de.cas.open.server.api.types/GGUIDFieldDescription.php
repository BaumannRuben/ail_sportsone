<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a GGUIDField
     */
    class GGUIDFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var int
         *
         *										Sets/Gets the length of the GGUIDField.
         */
        public $length;

    }

}
