<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents the result of a query for several rows (e.g. by defining a query
     *				like "SELECT Name, ChristianName FROM Address"). Extends AbstractMassData  to transport localised displaynames.
     *	@see AbstractMassData
     */
    class MassQueryResult extends \de\cas\open\server\api\types\AbstractMassData {

        /**
         * @var array
         *
         *										Returns the displayNames returned by the query, i.e.
         *										the localized names of the fields to be displayed.
         */
        public $displayNames;

    }

}
