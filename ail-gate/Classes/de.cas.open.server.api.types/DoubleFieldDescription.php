<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a DoubleField
     */
    class DoubleFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var int
         *
         *										Sets/Returns the maximum precision of this field in the
         *										database
         */
        public $precision;

        /**
         * @var int
         *
         *										Sets/Returns the maximum scale of this field in the
         *										database
         */
        public $scale;

        /**
         * @var double
         *
         *                    Sets/Gets the minimum value of the DoubleField.
         */
        public $minValue;

        /**
         * @var double
         *
         *                    Sets/Gets the maximum value of the DoubleField.
         */
        public $maxValue;

    }

}
