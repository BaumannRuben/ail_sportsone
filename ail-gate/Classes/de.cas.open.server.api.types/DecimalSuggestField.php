<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        A container for a decimal suggest value in a generic DataObject
     */
    class DecimalSuggestField extends \de\cas\open\server\api\types\DecimalField {

    }

}
