<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        A container for an int suggest value in a generic DataObject
     */
    class IntSuggestField extends \de\cas\open\server\api\types\IntField {

    }

}
