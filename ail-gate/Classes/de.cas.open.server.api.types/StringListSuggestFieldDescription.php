<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Describes a String List Field.
     *        In a field of this type, multiple values can be selected from the suggested values
     */
    class StringListSuggestFieldDescription extends \de\cas\open\server\api\types\StringSuggestFieldDescription {

    }

}
