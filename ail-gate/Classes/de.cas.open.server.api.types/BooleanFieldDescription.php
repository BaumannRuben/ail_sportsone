<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a BooleanField
     */
    class BooleanFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

    }

}
