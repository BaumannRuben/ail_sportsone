<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a single row in a MassQueryResult object.
     *				Additionally this object has its own tags.
     */
    class MassDataObjectRow extends \de\cas\open\server\api\types\MassDataRow {

        /**
         * @var array
         *
         *										Returns the list of Tags that either
         *										replace (INSERT or if {@link #replaceTags} is
         *										<code>true</code>) the existing tags
         *										or that will be added to the existing (if {@link
         *										#replaceTags} is
         *										<code>false</code>). If no tags given,
         *										the tags of the parent MassUpdateData will be used.
         */
        public $tags;

        /**
         * @var boolean
         *
         *										Sets/Returns the flag that indicates if the existing
         *										tags will be replaced by the tags given
         *										in {@link #tags} or if the tags will be
         *										added to the existing ones. In case of a batch INSERT
         *										this flag will be neglected.
         */
        public $replaceTags;

        /**
         * @var boolean
         *
         *										Sets/Returns the flag that indicates if the existing
         *										permissions will be replaced by the permissions given
         *										in {@link #permissions} or if the permissions will be
         *										added to the existing ones. In case of a batch INSERT
         *										this flag will be neglected.
         */
        public $replacePermissions;

        /**
         * @var string
         *
         *										Sets/Returns the new foreign edit permission
         *										restriction (i.e. the maximum permission a user can
         *										inherit by foreign edit permission).
         */
        public $foreignEditPermissionRestriction;

        /**
         * @var array
         *
         *										Returns the list of DataObjectPermissions that either
         *										replace (INSERT or if {@link #replacePermissions} is
         *										<code>true</code>) the existing permissions
         *										or that will be added to the existing (if {@link
         *										#replacePermissions} is
         *										<code>false</code>).
         */
        public $permissions;

    }

}
