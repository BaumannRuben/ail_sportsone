<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a TextField
     */
    class TextFieldDescription extends \de\cas\open\server\api\types\StringFieldDescription {

        /**
         * @var int
         *
         *										If the field's content exceeds this length, the content
         *										will be written to the overflow table
         *										(TextFieldConstants). This length is also the maximum
         *										length of the field's content that is returned in a
         *										MassQueryResult.
         *	@see MassQueryResult
         */
        public $overflowLength;

    }

}
