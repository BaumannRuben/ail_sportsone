<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Tag containing a localized value.
     */
    class LocalizedTag extends \de\cas\open\server\api\types\Tag {

        /**
         * @var string
         *
         *										Sets/Gets the localized value of the tag.
         */
        public $localizedValue;

    }

}
