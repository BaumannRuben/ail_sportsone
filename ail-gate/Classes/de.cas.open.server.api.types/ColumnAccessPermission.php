<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a permission on a column.
     */
    class ColumnAccessPermission {

        /**
         * @var string
         *Returns the permission's primary key.
         */
        public $GGUID;

        /**
         * @var string
         *
         *								Returns the type or table.
         */
        public $type;

        /**
         * @var string
         *
         *								Returns the name of the field or <code>null</code> for the default permission on all columns.
         */
        public $fieldName;

        /**
         * @var int
         *
         *								Returns the ID of the permissions owner to whom the permission is
         *								granted. Positive IDs: users, negative IDs: groups
         */
        public $accessorID;

        /**
         * @var int
         *
         *								Returns the edit permission (see {@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_READ_DENIED}
         *								or
         *								{@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_UPDATE_DENIED}
         *								or {@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_FULL_ACCESS}).
         */
        public $permission;

    }

}
