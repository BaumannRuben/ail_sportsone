<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: This business operation creates four files for the CAS
     *				Word plugin. The files are containing the data needed to create form
     *				letters(serveral letters created by one template document). It is important to
     *				release the checkout lock if the form letter editing is finished. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: GetFormLetterFilesAsZipResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFormLetterFilesAsZipResponse
     */
    class GetFormLetterFilesAsZipRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the document record which is used as form
         *										letter.
         */
        public $docGguid;

        /**
         * @var string
         *
         *										The proposed filename by the client, i.e.
         *										'teamCRMv2Mailing'. Do not append an file type! This
         *										filename is used for the generated files. An illegal
         *										argument expection is thrown if the filename is invalid
         *										or longer than 0xFFFF bytes.
         */
        public $baseFilename;

    }

}
