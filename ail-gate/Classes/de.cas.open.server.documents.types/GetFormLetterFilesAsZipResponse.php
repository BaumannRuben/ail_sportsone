<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: This business operation creates four files for the CAS
     *				Word plugin. The files are containing the data needed to create form
     *				letters(serveral letters created by one template document). It is important to
     *				release the checkout lock if the form letter editing is finished. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetFormLetterFilesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFormLetterFilesRequest
     */
    class GetFormLetterFilesAsZipResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *										Sets/Returns the zip file content. It contains all
         *										generated files like ini, titles, data, fields and doc.
         */
        public $zipFile;

        /**
         * @var string
         *
         *										The name of the document file which is needed to open
         *										the document with Word. In general, it contains the
         *										keyword.
         */
        public $fileNameOfDocument;

    }

}
