<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\ResponseObject: Resets the checkout state of a file if the user
     *		    holds the file lock or has edit permissions at least. An admin can reset file locks of other users.
     *		    No return value at resetting checkout state.
     *		    Corresponding \de\cas\open\server\api\types\RequestObject: ResetCheckOutStateAndSaveDocumentRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ResetCheckOutStateAndSaveDocumentRequest
     */
    class ResetCheckOutStateAndSaveDocumentResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
