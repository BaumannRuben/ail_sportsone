<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *				TODO...What does the business logic? Corresponding \de\cas\open\server\api\types\RequestObject:
     *				GetDocumentVersionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetDocumentVersionsResponse
     */
    class GetDocumentVersionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the corresponding document
         *										record. All document versions of this documen record
         *										GGUID are retrieved.
         */
        public $documentGGUID;

    }

}
