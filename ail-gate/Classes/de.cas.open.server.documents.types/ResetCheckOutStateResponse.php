<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\ResponseObject: Resets the checkout state of a file if the user
     *		    holds the file lock or has edit permissions at least. An admin can reset file locks of other users.
     *		    Corresponding \de\cas\open\server\api\types\RequestObject: ResetCheckOutStateRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ResetCheckOutStateRequest
     */
    class ResetCheckOutStateResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *								    This data object reference can be used to get the saved
         *								    \de\cas\open\server\api\types\DataObject.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $savedDataObject;

    }

}
