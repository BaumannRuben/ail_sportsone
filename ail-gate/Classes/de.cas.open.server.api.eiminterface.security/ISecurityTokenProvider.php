<?php

namespace de\cas\open\server\api\eiminterface\security {

    /**
     * Interface that enables getting of the current ISecurityToken.
     *
     * @package de\cas\open\server\api
     * @subpackage eiminterface\security
     */
    interface ISecurityTokenProvider {

        /**
         * Returns an appropriate ISecurityToken for the supplied SOAP action.
         *
         * @param string $soapAction the SOAP action of the request
         * @return \de\cas\open\server\api\eiminterface\security\ISecurityToken
         */
        public function getSecurityToken($soapAction);
    }
}
