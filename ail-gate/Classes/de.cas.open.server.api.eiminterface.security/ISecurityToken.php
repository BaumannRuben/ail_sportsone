<?php

namespace de\cas\open\server\api\eiminterface\security {

    /**
     * Interface that must be implemented by classes that want to supply additional security token mechanisms for the WS-Security SOAP header.
     *
     * @package de\cas\open\server\api
     * @subpackage eiminterface\security
     */
    interface ISecurityToken {

        /**
         * Returns the WS-Security SOAP header.
         *
         * @return \SoapHeader
         */
        public function getSecurityHeader();
    }
}
