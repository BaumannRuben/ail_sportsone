<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     *				Represents a dependent objecttype, i.e. objects of this type depend on
     *				a superior object. When the superior object is purged, the dependent
     *				objects are purged, too. The server logic synchronizes the permission.
     */
    class DependentObjectTypeDefinition extends \de\cas\open\server\datadefinition\types\ObjectTypeDefinition {

    }

}
