<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    abstract class DataTypeDefinitionBase {

        /**
         * @var unknown
         *
         */
        public $fieldList;

        /**
         * @var array
         *
         */
        public $field;

        /**
         * @var array
         *
         */
        public $valueListField;

        /**
         * @var unknown
         *
         */
        public $indexList;

        /**
         * @var array
         *
         */
        public $index;

    }

}
