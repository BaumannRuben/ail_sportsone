<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns a tag group by its GGUID. Corresponding \de\cas\open\server\api\types\RequestObject: GetTagGroupRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetTagGroupRequest
     */
    class GetTagGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagGroupTransferable
         *The requested tag group.
         */
        public $tagGroup;

    }

}
