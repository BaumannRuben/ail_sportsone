<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business
     *				operation that returns the available
     *				\de\cas\open\server\api\types\PrivilegedOperationSets.
     *				<br/>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetPrivilegedOperationSetsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\PrivilegedOperationSet
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPrivilegedOperationSetsResponse
     */
    class GetPrivilegedOperationSetsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
