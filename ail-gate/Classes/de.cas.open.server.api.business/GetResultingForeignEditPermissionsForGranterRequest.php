<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business operation that returns the ForeignEditPermissions granted by a permission owner or one of its
     *	 				super groups. The members of groups, a ForeignEditPermission is granted to, are
     *	 				added to.<br/>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetResultingForeignEditPermissionsForGranterResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ForeignEditPermission
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetResultingForeignEditPermissionsForGranterResponse
     */
    class GetResultingForeignEditPermissionsForGranterRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Returns the granter's ID. May be an user or a group.
         */
        public $granterID;

        /**
         * @var array
         *
         *										Returns the objecttypes to return the permissions for.
         */
        public $objectTypes;

    }

}
