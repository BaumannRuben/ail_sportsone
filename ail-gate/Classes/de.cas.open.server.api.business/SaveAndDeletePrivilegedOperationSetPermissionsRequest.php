<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business
     *				operation that saves and deletes
     *				\de\cas\open\server\api\types\PrivilegedOperationSetPermissions.
     *				<br/>
     *				Throws a DLE:SECURITY_BAD_CREDENTIALS if user isn't admin or userId
     *				isn't her-/himself.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				SaveAndDeletePrivilegedOperationSetPermissionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\PrivilegedOperationSetPermission
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveAndDeletePrivilegedOperationSetPermissionsResponse
     */
    class SaveAndDeletePrivilegedOperationSetPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Returns the \de\cas\open\server\api\types\PrivilegedOperationSetPermissions
         *										that shall be saved.
         *	@see \de\cas\open\server\api\types\PrivilegedOperationSetPermission
         */
        public $permissionsToSave;

        /**
         * @var array
         *
         *										Returns the GGUIDs of the \de\cas\open\server\api\types\PrivilegedOperationSetPermissions
         *										that shall be deleted.
         *	@see \de\cas\open\server\api\types\PrivilegedOperationSetPermission
         */
        public $gguidsPermissionsToDelete;

    }

}
