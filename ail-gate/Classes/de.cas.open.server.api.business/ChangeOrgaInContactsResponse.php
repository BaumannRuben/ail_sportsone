<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: ChangeOrgaInContactsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ChangeOrgaInContactsRequest
     */
    class ChangeOrgaInContactsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var int
         *
         *										Sets/Returns the affected contacts in this operation
         */
        public $contactsAffected;

    }

}
