<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject
     *				of the business operation that returns the ColumnAccessPermissions transitively, i.e. according to the
     *				passed <code>objectType</code>, the permissionOwner
     *				and all his/her super groups.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetColumnAccessPermissionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ColumnAccessPermission
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetColumnAccessPermissionsResponse
     */
    class GetColumnAccessPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Returns the ID of the accessor.
         */
        public $permissionOwnerId;

        /**
         * @var string
         *
         *										Returns the objecttype.
         */
        public $objectType;

    }

}
