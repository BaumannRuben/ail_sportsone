<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Marks the records with the given gguids as not
     *				duplicates of each other. Corresponding \de\cas\open\server\api\types\ResponseObject: MarkAsFalsePositiveResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see MarkAsFalsePositiveResponse
     */
    class MarkAsFalsePositiveRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Contains the gguids to be marked as false duplicates.
         */
        public $gguidList;

    }

}
