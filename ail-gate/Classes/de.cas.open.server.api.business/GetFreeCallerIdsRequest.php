<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *			Returns all free caller IDs. One of them can be used as a propose for a
     *	 		GetNewCallerIdRequest.
     *			The GUI needs to show which caller IDs are available or to do an input validation.
     *	 		Administrator permissions required.
     *			Corresponding \de\cas\open\server\api\types\ResponseObject: GetFreeCallerIdsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFreeCallerIdsResponse
     */
    class GetFreeCallerIdsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
