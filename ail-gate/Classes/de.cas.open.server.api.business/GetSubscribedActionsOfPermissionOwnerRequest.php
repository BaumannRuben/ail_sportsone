<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Used to retrieve subscriptions of an user/group.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetSubscribedActionsOfPermissionOwnerResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSubscribedActionsOfPermissionOwnerResponse
     */
    class GetSubscribedActionsOfPermissionOwnerRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Use NULL to retrieve subscriptions of the authenticated user.
         *										If the value isn't empty, an (action) admin can retrieve a list of subscription of other users or groups.
         */
        public $permissionsOwnerId;

    }

}
