<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject that transports failures.
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class SavedAndFailedLinksContainer extends \de\cas\open\server\api\business\MassOperationResultResponse {

        /**
         * @var array
         *
         *										Returns the successfully saved links.
         */
        public $savedLinks;

    }

}
