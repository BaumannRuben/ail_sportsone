<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Deletes the phone prefix of the client.
     *      This action deactivates the voice feature for the client.
     *      Administrator permissions required.
     *		<p>Possible fault codes explicitly thrown by this operation are:
     *			<ul>
     *				<li>BusinessException - BUSINESS.NO_PHONE_PREFIX_SET It is not possible to delete the phone prefix if no one is set</li>
     *			</ul>
     *		</p>
     *		Corresponding \de\cas\open\server\api\types\ResponseObject: DeletePhonePrefixForClientResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeletePhonePrefixForClientResponse
     */
    class DeletePhonePrefixForClientRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
