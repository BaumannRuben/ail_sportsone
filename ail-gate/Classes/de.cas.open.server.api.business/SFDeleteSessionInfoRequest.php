<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *      		Deletes a session information entry with the same sessionId value.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: SFDeleteSessionInfoResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SFDeleteSessionInfoResponse
     */
    class SFDeleteSessionInfoRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The id of the session to delete.
         */
        public $sessionId;

    }

}
