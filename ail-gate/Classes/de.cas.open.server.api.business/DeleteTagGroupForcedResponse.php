<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Deletes a tag group and all contained tags, their
     *				relations to data objects and all relations to data object types of the tag
     *				group. Corresponding \de\cas\open\server\api\types\RequestObject: DeleteTagGroupForcedRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteTagGroupForcedRequest
     */
    class DeleteTagGroupForcedResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
