<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that fetches a system property by
     *				key. The value returned is the value of the property.
     */
    class GetSystemPropertyResponse extends \de\cas\open\server\api\business\GetPropertyResponse {

    }

}
