<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.ResponseObject} containing the requested link. Corresponding \de\cas\open\server\api\types\RequestObject: GetLinkByGuidRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetLinkByGuidRequest
     */
    class GetLinkByGuidResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\LinkObject
         *Gets the link object.
         */
        public $link;

    }

}
