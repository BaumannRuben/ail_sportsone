<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of business operation that checks for company duplicates
     *				of a given address. \de\cas\open\server\api\types\RequestObject: CheckForCompanyContactDuplicatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckForCompanyContactDuplicatesRequest
     */
    class CheckForCompanyContactDuplicatesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the list of GGUIDs of possible company contact
         *										duplicates.
         */
        public $gguidsCompanyDuplicates;

    }

}
