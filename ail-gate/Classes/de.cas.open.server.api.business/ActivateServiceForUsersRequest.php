<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> activate external service for users.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				ActivateServiceForUsersResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ActivateServiceForUsersResponse
     */
    class ActivateServiceForUsersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets the service id , that should be activated for users.
         */
        public $serviceID;

        /**
         * @var array
         *
         *										Users to activate for service.
         */
        public $users;

    }

}
