<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Retrieves the image bytes for a given object.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetImageForObjectRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetImageForObjectRequest
     */
    class GetImageForObjectResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *										Sets/Returns the bytes of the image.
         */
        public $ImageBytes;

        /**
         * @var string
         *
         *										Sets/Returns the file extension for the image, e.g.
         *										'jpg' or 'bmp'.
         */
        public $ImageFileExtension;

    }

}
