<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.RequestObject} for saving links between records. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveLinkResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveLinkResponse
     */
    class SaveLinkRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Sets/Returns the links to establish.
         */
        public $links;

    }

}
