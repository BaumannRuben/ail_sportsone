<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business operation
     *				that saves and deletes possible \de\cas\open\server\datadefinition\types\SelectionValues of a \de\cas\open\server\api\types\SelectionValueListField.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: SavePossibleSelectionValuesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\datadefinition\types\SelectionValue
     *	@see \de\cas\open\server\api\types\SelectionValueListField
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SavePossibleSelectionValuesRequest
     */
    class SavePossibleSelectionValuesResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
