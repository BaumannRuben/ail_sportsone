<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject for the business operation that checks if single contact
     *				duplicates exist for the given address. Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				CheckForSingleContactDuplicatesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckForSingleContactDuplicatesResponse
     */
    class CheckForSingleContactDuplicatesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Sets/Returns the guids of the addresses that will be checked.
         */
        public $singleContactsGuids;

    }

}
