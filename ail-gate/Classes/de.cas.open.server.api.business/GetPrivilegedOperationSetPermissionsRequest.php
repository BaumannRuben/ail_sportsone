<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business
     *				operation that returns the granted
     *				\de\cas\open\server\api\types\PrivilegedOperationSetPermissions.
     *				<br/>
     *				Throws a DLE:SECURITY_BAD_CREDENTIALS if user isn't admin or userId
     *				isn't her-/himself.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetPrivilegedOperationSetPermissionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\PrivilegedOperationSetPermission
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPrivilegedOperationSetPermissionsResponse
     */
    class GetPrivilegedOperationSetPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Returns the permission owner the \de\cas\open\server\api\types\PrivilegedOperationSetPermissions
         *										shall
         *										be returned for. If <code>null</code> all defined permissions will be returned.
         *	@see \de\cas\open\server\api\types\PrivilegedOperationSetPermission
         */
        public $permissionOwnerId;

    }

}
