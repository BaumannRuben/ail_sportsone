<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business
     *				operation that returns the granted
     *				\de\cas\open\server\api\types\PrivilegedOperationSetPermissions.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetPrivilegedOperationSetPermissionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\PrivilegedOperationSetPermission
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPrivilegedOperationSetPermissionsRequest
     */
    class GetPrivilegedOperationSetPermissionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the granted \de\cas\open\server\api\types\PrivilegedOperationSetPermissions.
         *	@see \de\cas\open\server\api\types\PrivilegedOperationSetPermission
         */
        public $permissions;

    }

}
