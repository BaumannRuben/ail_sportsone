<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\RequestObject of the
     *        \de\cas\open\server\business\BusinessOperation to retrieve the
     *        primary link history for the given DataObjectType.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetPrimaryLinkHistoryResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPrimaryLinkHistoryResponse
     */
    class GetPrimaryLinkHistoryRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The DataObjectType for which the primary link history should be retrieved.
         */
        public $dataObjectType;

    }

}
