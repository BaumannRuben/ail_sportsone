<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the last used \de\cas\open\server\api\types\DataObjects
     *        for the given object type and scope in an ordered form (most recent used one at first
     *        position). Corresponding \de\cas\open\server\api\types\ResponseObject: GetLastUsedDataObjectsResponse
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetLastUsedDataObjectsResponse
     */
    class GetLastUsedDataObjectsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $scope;

        /**
         * @var array
         *
         *                    The columns to select and return in the response. If no column is
         *                    specified, all are returned.
         */
        public $selectColumns;

    }

}
