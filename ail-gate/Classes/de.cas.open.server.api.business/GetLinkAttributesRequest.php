<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *		    @link de.cas.open.server.api.types.RequestObject} for getting linkattribute(s) between source and linked records.
     *		    Corresponding \de\cas\open\server\api\types\ResponseObject: GetLinkAttributesResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetLinkAttributesResponse
     */
    class GetLinkAttributesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the link's source record.
         */
        public $sourceRecordGuid;

        /**
         * @var array
         *
         *                    Sets/Returns the GGUID of the link's target record.
         */
        public $linkedRecordGuids;

    }

}
