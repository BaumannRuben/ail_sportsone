<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Contains the selection part of the view whose ID has been passed to GetViewsSelectionRequest.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetViewsSelectionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetViewsSelectionRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class GetViewsSelectionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Gets/Sets the selection (WHERE part) of the view that
         *										has been requested. E.g. "address.Name = 'Hubert'".
         */
        public $selectionPart;

        /**
         * @var string
         *
         *										Gets/Sets the teamFilter part of the view that has been
         *										requested. E.g. "TEAMFILTER(appointment ; 15 ;
         *										'PARTICIPANT')".
         */
        public $teamFilterPart;

    }

}
