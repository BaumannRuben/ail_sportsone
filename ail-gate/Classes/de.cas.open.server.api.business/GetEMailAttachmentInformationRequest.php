<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Retrieves information about the attachments of an eMail.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetEMailAttachmentInformationResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetEMailAttachmentInformationResponse
     */
    class GetEMailAttachmentInformationRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the eMail object the
         *										attachment information shall be retrieved for.
         */
        public $eMailGGUID;

        /**
         * @var boolean
         *
         *										Flag indicating if inline attachment information shall
         *										be retrieved (flag set to 'true')
         */
        public $getInlineAttachments;

    }

}
