<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains a matching tag or null if no tag could be found. Corresponding \de\cas\open\server\api\types\RequestObject: FindSystemTagByValueRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see FindSystemTagByValueRequest
     */
    class FindSystemTagByValueResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagTransferable
         *The matching tag.
         */
        public $tag;

    }

}
