<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Provides display information about the client of the currently logged in user.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetClientDisplayInformationResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetClientDisplayInformationResponse
     */
    class GetClientDisplayInformationRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
