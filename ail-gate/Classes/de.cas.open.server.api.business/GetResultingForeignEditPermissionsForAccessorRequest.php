<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business operation that returns the ForeignEditPermissions
     *				 granted to an accessor for a list of objecttypes. The return value contains:
     *				 <ul>
     *				 <li>Permissions directly granted from a user to the accessor.</li>
     *				 <li>Permissions directly granted from a group to the accessor. These permissions will be
     *				 copied for every member of the granting group.</li>
     *				 <li>Permissions directly granted from a user to a super group of the accessor.</li>
     *				 <li>Permissions directly granted from a group to a super group of the accessor. These
     *				 permissions will be copied for every member of the granting group.</li>
     *				 </ul>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetResultingForeignEditPermissionsForAccessorResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ForeignEditPermission
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetResultingForeignEditPermissionsForAccessorResponse
     */
    class GetResultingForeignEditPermissionsForAccessorRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Returns the accessor's ID. May be an user or a group.
         */
        public $accessorID;

        /**
         * @var array
         *
         *										Returns the objecttypes to return the permissions for.
         */
        public $objectTypes;

    }

}
