<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Deletes a tag group. Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteTagGroupResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteTagGroupResponse
     */
    class DeleteTagGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *GGUID of the group to be deleted.
         */
        public $groupGUID;

    }

}
