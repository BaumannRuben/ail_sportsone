<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> activate external service for users.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				{@link "ActivateServiceForUsersRequest"}
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class ActivateServiceForUsersResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *											Gets users id's included to service.
         */
        public $serviceUsersID;

    }

}
