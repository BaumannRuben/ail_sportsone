<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that gets system-properties that
     *				belong to a propertyGroup.
     */
    class GetSystemPropertiesByGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $propertyGroup;

    }

}
