<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *			Returns all client sessions of an user, if they have been saved by SaveSessionInfoRequest. They are ordered by the insertTimestamp.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: SFGetSessionInfosResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SFGetSessionInfosResponse
     */
    class SFGetSessionInfosRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
