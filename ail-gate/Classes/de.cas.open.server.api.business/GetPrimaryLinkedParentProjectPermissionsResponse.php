<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\ResponseObject of the
     *        \de\cas\open\server\business\BusinessOperation to retrieve the
     *        permissions of primary linked parent project.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetPrimaryLinkedParentProjectPermissionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPrimaryLinkedParentProjectPermissionsRequest
     */
    class GetPrimaryLinkedParentProjectPermissionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    A List of the permissions of the parent project.
         */
        public $permissions;

        /**
         * @var string
         *
         *                    The ForeignEditPermission of the parent project.
         */
        public $foreignEditPermissionRestrictions;

    }

}
