<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business
     *				operation that saves and deletes
     *				\de\cas\open\server\api\types\PrivilegedOperationSetPermissions.
     *				<br/>
     *				Throws a DLE:SECURITY_BAD_CREDENTIALS if user isn't admin or userId
     *				isn't her-/himself.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: SaveAndDeletePrivilegedOperationSetPermissionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\PrivilegedOperationSetPermission
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveAndDeletePrivilegedOperationSetPermissionsRequest
     */
    class SaveAndDeletePrivilegedOperationSetPermissionsResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
