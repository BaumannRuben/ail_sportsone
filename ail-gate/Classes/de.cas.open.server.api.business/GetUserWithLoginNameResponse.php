<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response Object to receive a detailed user object that includes the user's loginname.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetUserWithLoginNameRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetUserWithLoginNameRequest
     */
    class GetUserWithLoginNameResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\UserWithUserName
         *The requested user details.
         */
        public $user;

    }

}
