<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Contains the maximum available permission that is granted to the authenticated user on the requested dataobject.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetPermissionOnDataObjectRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPermissionOnDataObjectRequest
     */
    class GetPermissionOnDataObjectResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var int
         *
         *                    The maximum available permission granted to the
         *                    authenticated user on the requested dataobject.
         */
        public $permission;

    }

}
