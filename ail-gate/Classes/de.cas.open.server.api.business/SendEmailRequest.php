<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Sends an e-mail to one or several recipients. No \de\cas\open\server\api\types\ResponseObject.<br/> Possible BusinessException codes are:<br/>
     *				<ul> <li>BUSINESS.NOTIFICATION: For serious exception without the
     *				possibility for a client to react in a sensible way..
     *				<li>BUSINESS.NOTIFICATION.ILLEGAL_ADDRESS_FORMAT:An e-mail address has an
     *				illegal format, i.e. "jack#jones.de". </li>
     *				<li>BUSINESS.NOTIFICATION.UNSUPPORTED_ENCODING: If the character encoding
     *				is not not supported, i.e. 'nameOfSender' argument with at least one special
     *				character. </li> <li>BUSINESS.NOTIFICATION.TOO_MUCH_MAILS_SENT: If
     *				an user has sent to much e-mails in a time interval (SPAM protection).
     *				</li> <li>BUSINESS.NOTIFICATION.HTML_PARSING_FAILED: If the user
     *				sends an HTML e-mail, the text will be additionally converted into a plain text
     *				e-mail. If the conversion fails, this exception code is returned. </li>
     *				<li>BUSINESS.NOTIFICATION.ATTACHMENT_TOO_BIG: The size of all attachments
     *				is limited to avoid DoS attacks and to protected the recipients against huge
     *				e-mails. </li> </ul>
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class SendEmailRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The subject ("header") of the e-mail. This value is
         *										mandatory.
         */
        public $subject;

        /**
         * @var string
         *
         *										The e-mail message. An HTML message must start with
         *										<html>.
         */
        public $message;

        /**
         * @var array
         *The "To" (primary) recipients.
         */
        public $toList;

        /**
         * @var array
         *
         *										The "Cc" (carbon copy) recipients.
         */
        public $ccList;

        /**
         * @var array
         *
         *										The "Bcc" (blind carbon copy) recipients.
         */
        public $bccList;

        /**
         * @var array
         *
         *										The attachments to append. A maximum of three
         *										attachments is set.
         */
        public $attachments;

    }

}
