<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of the business operation that returns the status and/or
     *				result of an asynchronous business operations started by RunAsynchronouslyRequest. Corresponding \de\cas\open\server\api\types\RequestObject: GetAsynchronousOperationStatusRequest. If an operation is finished the result
     *				can be retrieved just once, afterwards the status is destroyed.
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see RunAsynchronouslyRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAsynchronousOperationStatusRequest
     */
    class GetAsynchronousOperationStatusResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Sets/Returns the operation's status.
         */
        public $state;

        /**
         * @var double
         *
         *										Sets/Returns progress of the operation. The progress is
         *										the quotient of the current item that is processed and
         *										the total number of items and it ranges from 0 to 1.
         *										The progress can be <code>null</code> if
         *										progress tracking is not supported by the operation or
         *										the operation has not processed the first item (for
         *										example if the operation is not started yet).
         */
        public $progress;

        /**
         * @var \de\cas\open\server\api\types\ResponseObject
         *
         *										Sets/Returns the response of the operation if the
         *										operation is finished. If the operation is not finished
         *										yet the <code>operationResponse</code> is
         *										<code>null</code>.
         */
        public $operationResponse;

        /**
         * @var \de\cas\open\server\api\types\GuidEIMExceptionContainer
         *
         *										Sets/Returns the exception if an exception occured
         *										while executing the operation.
         */
        public $exception;

    }

}
