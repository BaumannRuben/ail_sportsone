<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Renames a tag group. Corresponding \de\cas\open\server\api\types\ResponseObject: RenameTagGroupResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see RenameTagGroupResponse
     */
    class RenameTagGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *									Sets/Returns the GGUID of the tag group to be renamed.
         */
        public $oldTagGroupGUID;

        /**
         * @var \de\cas\open\server\api\types\InternationalTagGroupTransferable
         *
         *									Sets/Returns the renamed tag group.
         */
        public $renamedTagGroup;

    }

}
