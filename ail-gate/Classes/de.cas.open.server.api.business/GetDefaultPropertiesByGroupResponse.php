<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that gets default-properties that
     *				belong to a propertyGroup.
     */
    class GetDefaultPropertiesByGroupResponse extends \de\cas\open\server\api\business\GetPropertiesByGroupResponse {

    }

}
