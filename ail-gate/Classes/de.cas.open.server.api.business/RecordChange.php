<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *					A bean containing changes related to a record.
     */
    class RecordChange {

        /**
         * @var string
         *
         *								The GUID of the record.
         */
        public $recordGuid;

        /**
         * @var string
         *
         *								The type of the change.
         */
        public $changeType;

        /**
         * @var array
         *
         *								A set of fields that were changed on this record. May be empty or null.
         */
        public $changedFields;

        /**
         * @var boolean
         *
         *								True if permissions were changed, false otherwise.
         */
        public $permissionChanged;

        /**
         * @var boolean
         *
         *								True if tags were changed, false otherwise.
         */
        public $tagsChanged;

        /**
         * @var int
         *
         *                The journal id of the record.
         */
        public $journalId;

    }

}
