<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object to receive a detailed user object that includes the user's loginname.
     *				This business operation can only be called by administrators.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetUserWithLoginNameResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetUserWithLoginNameResponse
     */
    class GetUserWithLoginNameRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										The id of the requested user.
         */
        public $id;

    }

}
