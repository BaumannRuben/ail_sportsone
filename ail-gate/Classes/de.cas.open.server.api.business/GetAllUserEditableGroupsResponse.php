<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Returns a list of TagGroups that are usereditable. Corresponding \de\cas\open\server\api\types\RequestObject: GetAllUserEditableGroupsRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllUserEditableGroupsRequest
     */
    class GetAllUserEditableGroupsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *									Returns List of \de\cas\open\server\api\types\TagGroup
         *									objects that are usereditable.
         *	@see \de\cas\open\server\api\types\TagGroup
         */
        public $userEditableGroups;

    }

}
