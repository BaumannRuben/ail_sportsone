<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of business operations that do not need to transport
     *				data back to the caller.
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class EmptyResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
