<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.ResponseObject} for getting link attributes of a link.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetLinkAttributesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetLinkAttributesRequest
     */
    class GetLinkAttributesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns with the link attribute(s).
         */
        public $attributes;

    }

}
