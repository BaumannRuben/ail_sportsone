<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *				Returns true if an user is registered for an external service, else false.
     *				Throws an IllegalArgumentException if serviceId or userId is emptry.
     *				Throws a DLE:SECURITY_BAD_CREDENTIALS if user isn't admin or userId
     *				isn't her-/himself.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				IsUserActivatedForServiceResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see IsUserActivatedForServiceResponse
     */
    class IsUserActivatedForServiceRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Gets/Sets the data object to contain the merged result.
         *										serviceId is case-insensitive.
         */
        public $serviceId;

        /**
         * @var int
         *
         *										The data object to copy the links, tags and permissions from, into the objectToMerge, which will be
         *										finally deleted.
         */
        public $userId;

    }

}
