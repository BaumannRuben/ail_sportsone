<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that fetches a property as a List of
     *				String-values by key and group.
     */
    class GetPropertyAsListRequest extends \de\cas\open\server\api\business\GetPropertyRequest {

    }

}
