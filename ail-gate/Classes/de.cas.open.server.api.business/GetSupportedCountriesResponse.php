<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Gets the countries that are supported by the server..
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetSupportedCountriesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSupportedCountriesRequest
     */
    class GetSupportedCountriesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										A list of available countries in lowercase two-letter
         *										format with their translated display values.
         */
        public $countries;

    }

}
