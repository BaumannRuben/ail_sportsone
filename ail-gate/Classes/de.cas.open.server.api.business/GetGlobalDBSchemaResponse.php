<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of the business operation that returns the global schema
     *				(for information and debugging). Corresponding \de\cas\open\server\api\types\RequestObject: GetGlobalDBSchemaRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGlobalDBSchemaRequest
     */
    class GetGlobalDBSchemaResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *								Sets/Returns the global schema.
         */
        public $globalSchema;

    }

}
