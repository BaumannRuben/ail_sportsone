<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Deletes an item from the search history of the user. No
     *				\de\cas\open\server\api\types\ResponseObject.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class DeleteSearchHistoryItemRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The ID of the search history item that should be
         *										deleted.
         */
        public $viewId;

        /**
         * @var string
         *
         *										The viewType to which the search history item belongs.
         */
        public $viewType;

    }

}
