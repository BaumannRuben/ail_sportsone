<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Returns information about the logins of the current user to the server.
     *        \de\cas\open\server\api\types\RequestObject: {@link
     *        GetCurrentUsersLoginInformationRequest }
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class GetCurrentUsersLoginInformationResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $LoginData;

    }

}
