<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> provide user ids included to external service.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				{@link "GetServiceUsersRequest"}
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class GetServiceUsersResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *											Set user id's included to service.
         */
        public $serviceUsersID;

    }

}
