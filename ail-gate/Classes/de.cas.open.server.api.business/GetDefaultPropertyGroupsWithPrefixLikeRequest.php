<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: TODO: Purpose of the business logic. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: GetDefaultPropertyGroupsWithPrefixLikeResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetDefaultPropertyGroupsWithPrefixLikeResponse
     */
    class GetDefaultPropertyGroupsWithPrefixLikeRequest extends \de\cas\open\server\api\business\GetPropertyGroupsWithPrefixLikeRequest {

    }

}
