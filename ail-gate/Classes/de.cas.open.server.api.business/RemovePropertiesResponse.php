<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that removes properties contained in
     *				a TransferableEIMProperties-object that belong to a propertyGroup.
     */
    class RemovePropertiesResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
