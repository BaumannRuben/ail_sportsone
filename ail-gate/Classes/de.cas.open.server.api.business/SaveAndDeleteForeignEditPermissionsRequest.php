<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request Object to save a list of
     *				foreigneditpermissions. User that are not an admin may only save
     *				permissions that are granted by themselves.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				SaveAndDeleteForeignEditPermissionsResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveAndDeleteForeignEditPermissionsResponse
     */
    class SaveAndDeleteForeignEditPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										List of foreign edit permissions to save. Permissions with GGUID are updated, permissions without GGUID are inserted.
         */
        public $permissions;

        /**
         * @var array
         *
         *										List of the GGUIDs of foreign edit permissions to delete.
         */
        public $fepsToDeleteGUIDs;

    }

}
