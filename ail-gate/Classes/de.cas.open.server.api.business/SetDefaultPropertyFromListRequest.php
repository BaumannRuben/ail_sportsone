<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that sets a default property value by
     *				key and group. The value of the property is passed as a List of Strings.
     */
    class SetDefaultPropertyFromListRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $propertyKey;

        /**
         * @var array
         *
         */
        public $propertyValue;

        /**
         * @var string
         *
         */
        public $propertyGroup;

    }

}
