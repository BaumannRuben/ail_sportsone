<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object of the business operation that
     *				saves a group.
     */
    class SaveGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\Group
         *
         *										Sets/Gets the group that has been updated /
         *										saved.
         */
        public $savedGroup;

    }

}
