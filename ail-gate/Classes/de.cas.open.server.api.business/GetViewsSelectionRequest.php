<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Returns the selection part (WHERE statement) of the filter which is referenced by a view surrounded by brackets.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					<li>BUSINESS_VIEWS_VIEW_NOT_FOUND - If no view could be found for the supplied view ID.</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetViewsSelectionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetViewsSelectionResponse
     */
    class GetViewsSelectionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Gets/Sets the source table that should be used for the
         *										fields in the selection. E.g. having a statement "WHERE
         *										Name = 'Hubert' AND address.christianName = 'Peter'"
         *										and using the sourceTable 'addr' will result in the
         *										following result: "(addr.Name = 'Hubert' AND
         *										addr.christianName = 'Peter')". So an adjustment to a
         *										custom projection is possible.
         *										<br/>
         *										If an empty String is passed as sourceTable, the fields will not contain any sourcetable prefix.
         *										<br/>
         *										If <code>null</null> is passed as sourceTable, the original filter linked with the view will not be altered.
         */
        public $sourceTable;

        /**
         * @var string
         *
         *											Gets/Sets the type of the view for which the selection (WHERE part) should be returned.
         *											@see ViewType
         */
        public $viewType;

        /**
         * @var string
         *
         *										Gets/Sets the ID of the view for which the selection
         *										(WHERE part) should be returned.
         */
        public $viewId;

    }

}
