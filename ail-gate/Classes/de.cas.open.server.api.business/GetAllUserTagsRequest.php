<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object to obtain a list of all usertags represented by \de\cas\open\server\api\types\LocalizedTag objects. Corresponding \de\cas\open\server\api\types\ResponseObject: \de\cas\eim\api\business\GetAllUserTagsResponse
     *	@see \de\cas\open\server\api\types\LocalizedTag
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\eim\api\business\GetAllUserTagsResponse
     */
    class GetAllUserTagsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
