<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Removes the relation between a tag group and an object type. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: RemoveTagGroupFromObjectTypeResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see RemoveTagGroupFromObjectTypeResponse
     */
    class RemoveTagGroupFromObjectTypeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *GGUID of the group.
         */
        public $groupGUID;

        /**
         * @var string
         *
         *									ObjectType the group will be removed from.
         */
        public $objectType;

    }

}
