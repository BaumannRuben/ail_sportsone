<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Returns all subscribed permissions owners (i.e. users) of an action.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetSubscribedPermissionsOwnersResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSubscribedPermissionsOwnersResponse
     */
    class GetSubscribedPermissionsOwnersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Get permissions owners of this action.
         */
        public $actionGuid;

    }

}
