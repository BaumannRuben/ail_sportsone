<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Contains the old and new value (complete, full length) of a journal entry. They
     *				are returned as typed values (String, Date, ...). Corresponding GetMemoEntriesRequest
     *	@see GetMemoEntriesRequest
     */
    class GetMemoEntriesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\Field
         *
         *										Complete old value of the journal entry.
         */
        public $oldValue;

        /**
         * @var \de\cas\open\server\api\types\Field
         *
         *										Complete new value of the journal entry.
         */
        public $newValue;

    }

}
