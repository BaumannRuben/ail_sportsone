<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Class representing a time slot with a start and an end date.
     */
    class TimeSlot {

        /**
         * @var unknown
         *
         *								Sets/Gets the start date of the time slot
         */
        public $start;

        /**
         * @var unknown
         *
         *								Sets/Gets the end date of the time slot
         */
        public $end;

        /**
         * @var int
         *
         *								Holds the OID that is available for the time slot
         */
        public $freeOID;

    }

}
