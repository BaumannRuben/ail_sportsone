<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Changes the password of a user. Either the user itself
     *				can change his password or an admin can change the password. No \de\cas\open\server\api\types\ResponseObject.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class ChangePasswordRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Gets/Sets the userId of the user whose password should
         *										be changed. If <code>null</code> the password
         *										of the currently authenticated user will be changed.
         */
        public $userId;

        /**
         * @var string
         *
         *										Gets/Sets the old Password of the user. Is not required
         *										for a caller who is logged in as admin.
         */
        public $oldPassword;

        /**
         * @var string
         *
         *										Gets/Sets the new password of the user. A value is
         *										required.
         */
        public $newPassword;

    }

}
