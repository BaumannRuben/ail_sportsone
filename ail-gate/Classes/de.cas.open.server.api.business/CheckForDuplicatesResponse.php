<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of business operation that checks for company duplicates
     *				of a given address. \de\cas\open\server\api\types\RequestObject: CheckForDuplicatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckForDuplicatesRequest
     */
    class CheckForDuplicatesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the list of GGUIDs of possible duplicates.
         */
        public $gguids;

    }

}
