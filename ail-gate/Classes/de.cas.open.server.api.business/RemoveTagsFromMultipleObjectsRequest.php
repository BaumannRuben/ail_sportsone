<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Removes tags from multiple data objects. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: RemoveTagsFromMultipleObjectsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see RemoveTagsFromMultipleObjectsResponse
     */
    class RemoveTagsFromMultipleObjectsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										List of Tags (LocalizedTag) that shall be removed.
         */
        public $tags;

        /**
         * @var string
         *
         *										Object type of the dataobjects.
         */
        public $objectType;

        /**
         * @var array
         *
         *										List of GGUIDs of data objects the given tag shall be
         *										removed from.
         */
        public $objectGUIDs;

        /**
         * @var unknown
         *
         *										Current DataObjectDescriptionVersion of the client.
         */
        public $dataObjectDescriptionVersion;

    }

}
