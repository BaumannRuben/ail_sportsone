<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that sets a system-property value by
     *				key. Nothing is returned.
     */
    class SetSystemPropertyResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
