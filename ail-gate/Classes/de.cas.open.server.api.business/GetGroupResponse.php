<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response Object to rreceive a detailed group object.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetGroupRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGroupRequest
     */
    class GetGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\Group
         *The requested group details.
         */
        public $group;

    }

}
