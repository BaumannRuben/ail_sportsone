<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Updates the last used \de\cas\open\server\api\types\DataObjects
     *        for the given object type and scope with the given guid. Corresponding
     *        \de\cas\open\server\api\types\ResponseObject: UpdateLastUsedDataObjectsResponse
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see UpdateLastUsedDataObjectsResponse
     */
    class UpdateLastUsedDataObjectsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $scope;

        /**
         * @var string
         *
         */
        public $objectGuid;

    }

}
