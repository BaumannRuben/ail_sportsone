<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the available XRM types for the given data object type.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetAvailableXrmTypesResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAvailableXrmTypesResponse
     */
    class GetAvailableXrmTypesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $dataObjectType;

    }

}
