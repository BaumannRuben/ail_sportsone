<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business
     *				operation that returns the available
     *				\de\cas\open\server\api\types\PrivilegedOperationSets.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetPrivilegedOperationSetsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\PrivilegedOperationSet
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPrivilegedOperationSetsRequest
     */
    class GetPrivilegedOperationSetsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the available \de\cas\open\server\api\types\PrivilegedOperationSets.
         *	@see \de\cas\open\server\api\types\PrivilegedOperationSet
         */
        public $privilegedOperationSets;

    }

}
