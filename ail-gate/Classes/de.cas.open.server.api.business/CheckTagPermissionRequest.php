<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Checks a number of data objects for sufficient rights to
     *				add or remove tags. Corresponding \de\cas\open\server\api\types\ResponseObject: CheckTagPermissionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckTagPermissionResponse
     */
    class CheckTagPermissionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Object type of the dataobjects.
         */
        public $objectType;

        /**
         * @var array
         *
         *										List of GGUIDs of data objects that shall be checked
         *										for sufficient permissions to add or remove tags.
         */
        public $objectGUIDs;

    }

}
