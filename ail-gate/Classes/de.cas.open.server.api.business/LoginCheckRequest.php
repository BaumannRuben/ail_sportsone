<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *			Checks if the requesting user has access to the database with the given
     *			login data. The busiess operation has no return value.
     *				<p>Possible fault which has to be handeld by the client with a
     *				   good error message:
     *				   NOTE: Because of the a missing infrastructure, always the same
     *				   result is returned (SECURITY_AUTHENTICATION_FAILED).
     *				   This will change in future and then the result will be as listed below.
     *					<ul>
     *					  <li>AuthenticationFailedException:SECURITY_AUTHENTICATION_FAILED - For invalid combinations of clientname, username, password</li>
     *					  <li>DATA_LAYER.CLIENT_DISABLED - The whole client has been disabled</li>
     *					  <li>DATA_LAYER.CLIENT_NOT_ACTIVATED - Client not activated after registration (email link)</li>
     *					  <li>DATA_LAYER.DBSERVER_DEACTIVATED - DBServer connection deactivated in global schema</li>
     *					  <li>DATA_LAYER.CLIENT_NOT_ACTIVATED - no explanation</li>
     *					  <li>TODO: user not activated - User has been deactivated</li>
     *					</ul>
     *				</p>
     *				There are other errors (Licene, Client not initialized, ...) which can be
     *				handeld in a generic way.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: LoginCheckResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see LoginCheckResponse
     */
    class LoginCheckRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
