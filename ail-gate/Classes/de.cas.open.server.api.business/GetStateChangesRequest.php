<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *					Business operation which provides information about changes of the state of records or links. <br/>
     *					Can be either used globally for all records of an object type or selectively on a set of given records.
     *					Additional filter criteria can be provided to get only the information that is required.
     *					Corresponding \de\cas\open\server\api\types\ResponseObject: GetStateChangesResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetStateChangesResponse
     */
    class GetStateChangesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *                    The minimal (lowest) journalId property defines the reference point from which state changes should be fetched. Use 0 for an initial request.
         *                    May be null in case the {@link #timestamp} property is set.
         */
        public $minJournalId;

        /**
         * @var int
         *
         *                    The maximal (highest) journalId property defines the reference point to which state changes should be fetched
         *                    including the change with the given journal id.
         *                    May be null in case of retrieving all journal ids.
         */
        public $maxJournalId;

        /**
         * @var unknown
         *
         *										The timestamp property defines the reference point from which state changes should be fetched. Set {@link #journalId} to 0 for an initial request.
         *										May be null in case the {@link #journalId} property is set. It is advisable to use the {@link #journalId} in preference over the {@link #timestamp} property,
         *										as it is more accurate.
         */
        public $timestamp;

        /**
         * @var int
         *
         *										The pageSize property defines the amount of state changes to be reported in a single response. As large responses can be quite slow especially
         *										over the webservice interface the pageSize is limited to a maximum of 1000 (which is the default in case the property is not set).
         */
        public $pageSize;

        /**
         * @var string
         *
         *										The objectType property defines the object type for which state changes will be returned and must not be null or empty.
         */
        public $objectType;

        /**
         * @var array
         *
         *										Optionally restricts the detection of state changes on a given set of records.
         */
        public $recordGuids;

        /**
         * @var array
         *
         *										Optionally restricts the detection of record state changes on a given set of fields.
         *										Has no meaning if change detection shouldn't be done on a record basis or if
         *										{@link #changeOperationFilter} doesn't contain {@link JournalChangeAction#UPDATE}.
         */
        public $fieldFilter;

        /**
         * @var array
         *
         *										Optionally restricts the operation-types of state changes to be considered (e.g. only new, only new and updated, ...)
         */
        public $changeOperationFilter;

        /**
         * @var array
         *
         *										Optionally restricts the changes being returned to certain types of entities like records (changes on fields, tags, record-deletion, -creation, ..),
         *										permissions on records, links (creation, deletion).
         */
        public $changeTypeFilter;

    }

}
