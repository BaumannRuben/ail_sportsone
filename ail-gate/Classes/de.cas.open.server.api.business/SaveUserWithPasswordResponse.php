<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object of the business operation that
     *				saves a user and his/her
     *				password.
     */
    class SaveUserWithPasswordResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\UserWithUserName
         *Sets/Gets the saved user.
         */
        public $savedUser;

    }

}
