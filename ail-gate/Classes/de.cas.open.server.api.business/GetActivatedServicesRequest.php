<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the services that are activated on the application server.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetActivatedServicesResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetActivatedServicesResponse
     */
    class GetActivatedServicesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
