<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> TODO [Albina.Pace]: Returns the appointment data object.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: AppointmentImportFromVCalRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AppointmentImportFromVCalRequest
     */
    class AppointmentImportFromVCalResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										The gguids of the imported, saved, appointments.
         */
        public $importedAppointmentsGguids;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										The created, unsaved, appointment. Only if the import
         *										just contained one appointment, otherwise the
         *										appointments are saved and their gguids returned.
         */
        public $importedAppointment;

    }

}
