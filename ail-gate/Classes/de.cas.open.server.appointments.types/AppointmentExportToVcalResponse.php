<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> TODO [Albina.Pace]: Purpose of the business logic.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: AppointmentExportToVcalRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AppointmentExportToVcalRequest
     */
    class AppointmentExportToVcalResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *The vCalendar Content.
         */
        public $vCalContent;

    }

}
