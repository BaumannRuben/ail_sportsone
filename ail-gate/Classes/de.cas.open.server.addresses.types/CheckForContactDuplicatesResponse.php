<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject for the business operation that checks if contact
     *        duplicates exist for a given contact object.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CheckForContactDuplicatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckForContactDuplicatesRequest
     */
    class CheckForContactDuplicatesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *                    Returns the list of GGUIDs of possible contact duplicates.
         */
        public $duplicateObjects;

    }

}
