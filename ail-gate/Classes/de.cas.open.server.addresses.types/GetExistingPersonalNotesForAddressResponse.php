<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject:
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetExistingPersonalNotesForAddressRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetExistingPersonalNotesForAddressRequest
     */
    class GetExistingPersonalNotesForAddressResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										Contains all Kontaktnotizen object for the given Address.
         *										Column: GGUID, OWNERNAME, NOTES2, GROUPGGUID
         *										The extra field: GROUPGGUID, indicates the assigned usergroup for the Kontaktnotizen
         */
        public $Result;

    }

}
