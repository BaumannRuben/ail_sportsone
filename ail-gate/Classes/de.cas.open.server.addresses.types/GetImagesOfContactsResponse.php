<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\ResponseObject: Returns address data,
     *		    which are selected by the query argument.
     *		    After the retrieval of the MassQueryResult, the business operation adds another column
     *		    called 'imageData'. This column contains the contact image as PNG image with a
     *		    scaled maximum resolution of 200x200. <br/>
     *		    Please always select the column ADDRESS.IMAGEGUID, which is needed to retrieve
     *		    the images. Corresponding
     *		    \de\cas\open\server\api\types\RequestObject: GetImagesOfContactsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetImagesOfContactsRequest
     */
    class GetImagesOfContactsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										Contact data and the contact image in the column 'imageData'.
         */
        public $contactsAndImages;

    }

}
