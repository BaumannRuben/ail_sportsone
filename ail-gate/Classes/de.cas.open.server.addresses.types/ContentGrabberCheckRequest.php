<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: The function tries to extract all address-related information from the parameter string
     *        CheckStr and returns them as formatted XML. For this purpose, it accesses the Webservice of the CAS by means of the internet, which is remotely
     *        available via https://contentgrabber.cas.de/Service?contact=. This address must be passed to the parameter. Unclassified content is returned in
     *        the XML element notClassified. The function is besides its return type fully equivalent to function ContentGrabberCheckDOM.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: ContentGrabberCheckResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ContentGrabberCheckResponse
     */
    class ContentGrabberCheckRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Arbitrary String that contains the address information.
         */
        public $CheckStr;

        /**
         * @var int
         *
         *										Type of the address to be created:
         *										SingleContact: 0
         *										Contact: 1
         *										Company: 2
         */
        public $ContactType;

    }

}
