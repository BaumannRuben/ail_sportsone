<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\exceptions\faults {

    /**
     * @package de\cas\open\server\api
     * @subpackage exceptions\faults
     *
     */
    class EIMFaultType {

        /**
         * @var string
         *
         */
        public $code;

        /**
         * @var string
         *
         */
        public $message;

        /**
         * @var string
         *
         */
        public $serverStackTrace;

        /**
         * @var unknown
         *
         */
        public $createdOn;

    }

}
