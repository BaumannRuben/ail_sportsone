<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Removes all states that are related to a certain clientId, collectionId and elementType.
     */
    class ClearStateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\mobilesync\types\ElementStateIdentifier
         *
         */
        public $stateIdentifier;

    }

}
