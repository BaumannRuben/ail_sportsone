<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Describes a permission for a field.
     */
    class FieldPermission {

        /**
         * @var string
         *
         */
        public $elementType;

        /**
         * @var string
         *
         */
        public $fieldName;

        /**
         * @var int
         *
         */
        public $fieldAccess;

    }

}
