<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        A tupel of client id and sync key.
     */
    class StateIdentifier {

        /**
         * @var string
         *
         */
        public $clientID;

        /**
         * @var string
         *
         */
        public $syncKey;

    }

}
