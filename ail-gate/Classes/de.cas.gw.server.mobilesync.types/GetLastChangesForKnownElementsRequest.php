<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Returns the change types for known elements.
     */
    class GetLastChangesForKnownElementsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\mobilesync\types\ElementStateIdentifier
         *
         */
        public $stateIdentifier;

        /**
         * @var \de\cas\gw\server\mobilesync\types\ChangeCriteria
         *
         */
        public $changeCriteria;

    }

}
