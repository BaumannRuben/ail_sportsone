<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Fetches the changed fields of an element starting from a given journalId.
     */
    class GetChangedFieldsSinceRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $elementId;

        /**
         * @var int
         *
         */
        public $fromJournalId;

    }

}
