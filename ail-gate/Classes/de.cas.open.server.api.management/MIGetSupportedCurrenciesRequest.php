<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the ManagementOperation that returns all supported currencies. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIGetSupportedCurrenciesResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetSupportedCurrenciesResponse
     */
    class MIGetSupportedCurrenciesRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

    }

}
