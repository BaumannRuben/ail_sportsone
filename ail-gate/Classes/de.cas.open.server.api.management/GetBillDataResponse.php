<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject:<br/>
     *				GetBill returns the data to create a bill for using teamCRM.
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject: GetBillDataRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see GetBillDataRequest
     */
    class GetBillDataResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *The positions of a bill, i.e. '4 active users'.
         */
        public $positions;

    }

}
