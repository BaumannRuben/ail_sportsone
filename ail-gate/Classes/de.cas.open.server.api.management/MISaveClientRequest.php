<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management operation that saves clients.
     *			<br/>Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MISaveClientResponse.
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MISaveClientResponse
     */
    class MISaveClientRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var \de\cas\open\server\api\types\Client
         *Sets/Returns the client to save.
         */
        public $client;

    }

}
