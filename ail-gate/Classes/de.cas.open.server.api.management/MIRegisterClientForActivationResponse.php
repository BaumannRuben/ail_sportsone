<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the management operation that
     *				registers a new client that must be activated afterwards.<br/>
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIRegisterClientForActivationRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIRegisterClientForActivationRequest
     */
    class MIRegisterClientForActivationResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var \de\cas\open\server\api\types\Client
         *Sets/Returns the registered client.
         */
        public $client;

    }

}
