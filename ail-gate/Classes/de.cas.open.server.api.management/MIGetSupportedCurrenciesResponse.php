<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the ManagementOperation that returns all supported currencies. Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIGetSupportedCurrenciesRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetSupportedCurrenciesRequest
     */
    class MIGetSupportedCurrenciesResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *Sets/Gets the currency list.
         */
        public $currencies;

    }

}
