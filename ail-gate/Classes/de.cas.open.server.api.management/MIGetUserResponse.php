<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementResponseObject of the \de\cas\open\server\management\ManagementOperation that
     *				retrieves a user from the database. Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIGetUserRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\management\ManagementOperation
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetUserRequest
     */
    class MIGetUserResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var \de\cas\open\server\api\types\UserWithUserName
         *
         *										Returns the user's ID.
         */
        public $user;

    }

}
