<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management logic that removes a client from the global schema and DELETES its database. No \de\cas\open\server\api\types\ManagementResponseObject.
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     */
    class MIRemoveClientRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *Sets/Returns the clientprefix of the client.
         */
        public $clientPrefix;

    }

}
