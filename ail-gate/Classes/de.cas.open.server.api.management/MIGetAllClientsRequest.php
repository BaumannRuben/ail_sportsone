<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management operation that returns all registered clients. <br />
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIGetAllClientsResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetAllClientsResponse
     */
    class MIGetAllClientsRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var boolean
         *Determines whether the last login date should be calculated for each client.
         */
        public $fillLoginDate;

    }

}
