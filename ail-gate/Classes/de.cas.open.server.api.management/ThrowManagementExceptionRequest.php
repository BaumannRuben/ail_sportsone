<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject: Provokes an exception for testing purposes. No ResponseObject.
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     */
    class ThrowManagementExceptionRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var boolean
         *If this value is set to true, the transaction will be thrown within a transaction.
         */
        public $throwWithinTransaction;

    }

}
