<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *Represents an activity that is relevant for the creation of a bill, e.g. the creation of user.
     */
    class BillDataEvent extends \de\cas\open\server\api\management\BillPosition {

        /**
         * @var string
         *Sets/Returns the GGUID of the BillData object.
         */
        public $GGUID;

        /**
         * @var unknown
         *Sets/Returns the time of the event.
         */
        public $date;

        /**
         * @var boolean
         *Sets/Returns the flag that indicates that this event has been backed up. A backup (= a copy) will be created every time a bill is created.
         */
        public $billBackup;

        /**
         * @var string
         *Sets/Returns a description of the event.
         */
        public $description;

        /**
         * @var string
         *Sets/Returns the GGUID of the object that was modified such that this event has been logged.
         */
        public $objectGuid;

        /**
         * @var string
         *Sets/Returns the user that issued the event.
         */
        public $insertUser;

    }

}
