<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementResponseObject: Carries all languages currently enabled by the system.
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIGetEnabledLanguagesRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetEnabledLanguagesRequest
     */
    class MIGetEnabledLanguagesResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *
         *										A list of available languages in lowercase two-letter
         *										format with their translated display values.
         */
        public $languages;

    }

}
