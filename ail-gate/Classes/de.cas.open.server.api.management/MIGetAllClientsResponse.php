<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the management operation that returns all registered clients. <br />
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIGetAllClientsRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetAllClientsRequest
     */
    class MIGetAllClientsResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *Returns all registered clients with their last login date.
         */
        public $Clients;

    }

}
