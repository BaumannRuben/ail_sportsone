<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject:<br/> activate external service for users.
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject:
     *				MIActivateServiceForUsersResponse
     *				<p>Proxy operation for the {link de.cas.open.server.business.BusinessOperation}
     *				invoked by \de\cas\open\server\api\business\ActivateServiceForUsersResponse</p>
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIActivateServiceForUsersResponse
     *	@see \de\cas\open\server\api\business\ActivateServiceForUsersResponse
     */
    class MIActivateServiceForUsersRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         *										Sets the client to manage.
         */
        public $client;

        /**
         * @var string
         *
         *										Sets the service id , that should be activated
         *										for users.
         */
        public $serviceID;

        /**
         * @var array
         *
         *										Users to activate for service.
         */
        public $users;

    }

}
