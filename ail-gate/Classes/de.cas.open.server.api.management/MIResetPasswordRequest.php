<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementRequestObject of the
     *				management operation that resets a user's
     *				password. You must provide
     *				the GGUID returned from the \de\cas\open\server\management\ManagementOperation that is invoked
     *				by MIGetResetPasswordGUIDRequest. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIResetPasswordResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\management\ManagementOperation
     *	@see MIGetResetPasswordGUIDRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIResetPasswordResponse
     */
    class MIResetPasswordRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         *										Returns the client of the user.
         */
        public $client;

        /**
         * @var string
         *
         *										Returns the loginname of the user whose password
         *										shall be reset.
         */
        public $loginName;

        /**
         * @var string
         *
         *										Returns the new password.
         */
        public $newPassword;

        /**
         * @var string
         *
         *										The GGUID that was returned by MIGetResetPasswordGUIDResponse.
         *	@see MIGetResetPasswordGUIDResponse
         */
        public $resetPasswordGUID;

    }

}
