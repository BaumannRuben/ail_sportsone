<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementRequestObject of the
     *				management operation that starts the process to reset a user's
     *				password. The operation returns a GGUID that must be used to
     *				actually change the password using MIResetPasswordRequest.
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject:
     *				MIGetResetPasswordGUIDResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIResetPasswordRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetResetPasswordGUIDResponse
     */
    class MIGetResetPasswordGUIDRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         *										Returns the client of the user.
         */
        public $client;

        /**
         * @var string
         *
         *										Returns the loginname of the user whose password
         *										shall be reset.
         */
        public $loginName;

    }

}
