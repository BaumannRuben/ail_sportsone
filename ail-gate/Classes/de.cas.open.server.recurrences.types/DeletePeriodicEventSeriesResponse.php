<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *					\de\cas\open\server\api\types\ResponseObject:<br/> Returns the deleted record guids that
     *					belonged to the deleted periodic series.
     *					<br/>Corresponding \de\cas\open\server\api\types\RequestObject: DeletePeriodicEventSeriesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeletePeriodicEventSeriesRequest
     */
    class DeletePeriodicEventSeriesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Gets the GGUIDs of the events that have been
         *                    deleted.
         */
        public $deletedEventGuids;

        /**
         * @var array
         *
         *                    Returns the GuidEIMExceptionContainers
         *                    that
         *                    contain the exception details why an event could not be
         *                    deleted.
         *	@see GuidEIMExceptionContainer
         */
        public $failedEventDeletions;

    }

}
