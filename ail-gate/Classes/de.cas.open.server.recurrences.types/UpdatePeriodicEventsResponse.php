<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Contains information, if the event that served as base for the event calculation is still part of the series.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: UpdatePeriodicEventsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see UpdatePeriodicEventsRequest
     */
    class UpdatePeriodicEventsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Sets/Gets the flag that determines if the base
         *                    event,
         *                    on which the calculation bases, is still part of the
         *                    resulting series.
         */
        public $baseEventPartOfSeries;

        /**
         * @var array
         *
         *                    Sets/Gets the guids of the records that were
         *                    deleted during the update of the recurrence.
         */
        public $deletedRecords;

        /**
         * @var array
         *
         *                    Sets/Gets the guids of the records that were
         *                    updated during the update of the recurrence.
         */
        public $updatedRecords;

    }

}
