<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> TODO [clemens.schneider]: Purpose of the business logic.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: GetPeriodicEventUpdateCandidatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPeriodicEventUpdateCandidatesRequest
     */
    class GetPeriodicEventUpdateCandidatesResponse extends \de\cas\open\server\recurrences\types\GetPeriodicEventCandidatesResponse {

        /**
         * @var array
         *
         *											Sets/Gets the permissions for the update candidates
         *											contained in the response. These are related to the
         *											update candidates by the order of the two lists and
         *											can be used for scheduling conflict checks.
         */
        public $permissionsForUpdateCandidates;

    }

}
