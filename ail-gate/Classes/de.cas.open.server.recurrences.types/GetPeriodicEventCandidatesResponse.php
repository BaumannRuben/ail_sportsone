<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Contains the
     *        calculated candidates for a periodic event
     *        and (if queried) the
     *        resulting conflicts. Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetPeriodicEventCandidatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPeriodicEventCandidatesRequest
     */
    class GetPeriodicEventCandidatesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *											Sets/Gets the resulting candidates for the periodic event.
         *											<br/>
         *											Contained fields per row are:
         *											<p>
         *												<ul>
         *													<li>The intermin GGUID of the event candidate.</li>
         *						              				<li>The intermin PERIOD_GUID of the event candidate.</li>
         *						              				<li>The intermin START_DT of the event candidate.</li>
         *						              				<li>The intermin END_DT of the event candidate.</li>
         *						              				<li>The intermin KEYWORD of the event candidate.</li>
         *												</ul>
         *											</p>
         */
        public $periodicEventsToBeCreated;

        /**
         * @var string
         *
         *											Sets/Gets the GGUID of the period to which the event candidates will belong.
         *											All event candidates will have this GGUID set as
         *											field PERIODGUID.
         */
        public $periodGuid;

        /**
         * @var boolean
         *
         *                    Sets/Gets the flag that determines if the base
         *                    event,
         *                    on which the calculation bases, is still part of the
         *                    resulting series.
         */
        public $baseEventPartOfSeries;

    }

}
