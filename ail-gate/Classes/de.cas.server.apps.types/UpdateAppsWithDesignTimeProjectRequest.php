<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: The request-object to update an apps with the files in uploaded designtime project zip.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: UpdateAppWithDesignTimeProjectResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see UpdateAppWithDesignTimeProjectResponse
     */
    class UpdateAppsWithDesignTimeProjectRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         */
        public $zip;

    }

}
