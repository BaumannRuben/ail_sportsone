<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class Extension {

        /**
         * @var array
         *
         */
        public $configurationElements;

        /**
         * @var string
         *
         */
        public $id;

        /**
         * @var string
         *
         */
        public $contributor;

    }

}
