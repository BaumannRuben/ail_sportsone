<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: The request-object to get flow and form mapping Xamls for the requested host app id.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetFlowAndFormMappingsForAppResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFlowAndFormMappingsForAppResponse
     */
    class GetFlowAndFormMappingsForAppRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Identifier for the App the resulting flow and form mapping Xamls should be computed for.
         */
        public $appId;

    }

}
