<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns Xamls for the flow and form mappings of the given App.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetFlowAndFormMappingsForAppRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFlowAndFormMappingsForAppRequest
     */
    class GetFlowAndFormMappingsForAppResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\server\apps\types\Flow
         *
         *                    The flow for the given App. Active fragment apps have already been merged into this flow.
         */
        public $flow;

        /**
         * @var \de\cas\server\apps\types\FormMappings
         *
         *                    The form mappings for the given App. Form mappings from active fragment apps
         *                    have already been merged into this form mapping.
         */
        public $formMappings;

    }

}
