<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class FormMapping {

        /**
         * @var string
         *
         */
        public $pageType;

        /**
         * @var string
         *
         */
        public $form;

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $when;

        /**
         * @var int
         *
         */
        public $ranking;

        /**
         * @var boolean
         *
         */
        public $default;

        /**
         * @var int
         *
         */
        public $minWidth;

        /**
         * @var int
         *
         */
        public $maxWidth;

        /**
         * @var int
         *
         */
        public $preferredRelativeWidth;

    }

}
