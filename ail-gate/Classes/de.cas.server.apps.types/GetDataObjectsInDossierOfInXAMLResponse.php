<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the DataObjects from the Dossier of the DataObject,
     *        specified by the GGUID parameter, in a silverlight xaml compatible format, as a ResourceDictionary.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetDataObjectsInDossierOfInXAMLRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetDataObjectsInDossierOfInXAMLRequest
     */
    class GetDataObjectsInDossierOfInXAMLResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         */
        public $DataObjectsXAML;

    }

}
