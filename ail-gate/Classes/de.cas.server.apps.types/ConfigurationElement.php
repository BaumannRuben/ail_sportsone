<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class ConfigurationElement {

        /**
         * @var string
         *
         */
        public $name;

        /**
         * @var string
         *
         */
        public $value;

        /**
         * @var array
         *
         */
        public $attributes;

        /**
         * @var array
         *
         */
        public $children;

    }

}
