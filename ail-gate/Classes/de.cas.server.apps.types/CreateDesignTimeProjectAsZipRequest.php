<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: The request-object to get the design time project as a zip file.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: CreateDesignTimeProjectAsZipResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CreateDesignTimeProjectAsZipResponse
     */
    class CreateDesignTimeProjectAsZipRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The DataObjects in the dossier of the DataObject specified by this GGUID
         *                    are returned in the design time project as example data.
         */
        public $GGUID;

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var array
         *
         */
        public $appGguids;

        /**
         * @var string
         *
         */
        public $safeprojectname;

    }

}
