<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: The request-object to get the apps, specified by a list of ids, as a zip file.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetAppsAsZipResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAppsAsZipResponse
     */
    class GetAppsAsZipRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         */
        public $appGguids;

    }

}
