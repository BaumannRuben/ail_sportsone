<?php
// This file has been automatically generated.

namespace de\cas\open\server\opportunities\types {

    /**
     * @package de\cas\open\server\opportunities
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets the list of products that can be added
     *        as positions to an opportunity.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetOpportunityPositionSelectionListResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetOpportunityPositionSelectionListResponse
     */
    class GetOpportunityPositionSelectionListRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    An additional WHERE-string
         */
        public $AdditionalWhereString;

        /**
         * @var int
         *
         *                    The first record
         */
        public $FirstRecord;

        /**
         * @var int
         *
         *                    The record count
         */
        public $RecordCount;

        /**
         * @var boolean
         *
         *                    Defines if all columns should be selected
         */
        public $SelectAllColumns;

        /**
         * @var array
         *
         *                    The columns to be selected
         */
        public $SelectColumns;

        /**
         * @var array
         *
         *                    The columns used to sort the result
         */
        public $OrderByColumns;

        /**
         * @var string
         *
         *                    Sets the team filter
         */
        public $Teamfilter;

    }

}
