<?php
// This file has been automatically generated.

namespace de\cas\open\server\opportunities\types {

    /**
     * @package de\cas\open\server\opportunities
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\ResponseObject for the businessoperation that returns all
     *		    opportunitypositions to a given opportunity.
     *		    Corresponding \de\cas\open\server\api\types\RequestObject: GetOpportunityPositionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetOpportunityPositionsRequest
     */
    class GetOpportunityPositionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										Sets/Returns the resultobject containing the
         *										opportunitypositions.
         */
        public $positions;

    }

}
