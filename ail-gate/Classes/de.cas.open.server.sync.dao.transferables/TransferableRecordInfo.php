<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableRecordInfo extends \de\cas\open\server\sync\dao\transferables\TransferableRecordId {

        /**
         * @var boolean
         *
         */
        public $deleted;

    }

}
