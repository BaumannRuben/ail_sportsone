<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferablePairContainer {

        /**
         * @var string
         *
         */
        public $id;

        /**
         * @var string
         *
         */
        public $leftId;

        /**
         * @var string
         *
         */
        public $rightId;

        /**
         * @var string
         *
         */
        public $leftCheckSum;

        /**
         * @var string
         *
         */
        public $rightCheckSum;

        /**
         * @var array
         *
         */
        public $leftSyncViewIds;

        /**
         * @var array
         *
         */
        public $rightSyncViewIds;

        /**
         * @var string
         *
         */
        public $failureType;

        /**
         * @var string
         *
         */
        public $failureMessage;

        /**
         * @var array
         *
         */
        public $leftAdditionalData;

        /**
         * @var array
         *
         */
        public $rightAdditionalData;

        /**
         * @var boolean
         *
         */
        public $deleted;

        /**
         * @var int
         *
         */
        public $errorCount;

    }

}
