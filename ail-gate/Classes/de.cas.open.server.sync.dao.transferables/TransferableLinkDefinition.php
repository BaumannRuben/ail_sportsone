<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableLinkDefinition {

        /**
         * @var string
         *
         */
        public $name;

        /**
         * @var string
         *
         */
        public $definition;

        /**
         * @var string
         *
         */
        public $linkConstraint;

        /**
         * @var string
         *
         */
        public $linkDirection;

    }

}
