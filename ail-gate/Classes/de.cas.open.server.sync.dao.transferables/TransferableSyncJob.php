<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableSyncJob {

        /**
         * @var string
         *
         */
        public $syncJobId;

        /**
         * @var string
         *
         */
        public $executionInterval;

        /**
         * @var unknown
         *
         */
        public $lastExecution;

        /**
         * @var array
         *
         */
        public $syncConfigurationIds;

    }

}
