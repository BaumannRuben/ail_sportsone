<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferablePairUpdate {

        /**
         * @var string
         *
         */
        public $recordId;

        /**
         * @var string
         *
         */
        public $changedRecordId;

        /**
         * @var string
         *
         */
        public $newCheckSum;

        /**
         * @var string
         *
         */
        public $foreignRecordChecksum;

        /**
         * @var array
         *
         */
        public $syncViewIds;

        /**
         * @var array
         *
         */
        public $additionalData;

    }

}
