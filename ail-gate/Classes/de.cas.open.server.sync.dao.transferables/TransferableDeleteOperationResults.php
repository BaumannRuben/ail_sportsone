<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableDeleteOperationResults {

        /**
         * @var array
         *
         */
        public $successfulRecordStatuses;

        /**
         * @var array
         *
         */
        public $failedRecordStatuses;

    }

}
