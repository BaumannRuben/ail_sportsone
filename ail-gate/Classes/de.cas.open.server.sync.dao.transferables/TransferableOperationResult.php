<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableOperationResult {

        /**
         * @var string
         *
         */
        public $recordGuid;

        /**
         * @var string
         *
         */
        public $changedRecordGuid;

        /**
         * @var int
         *
         */
        public $newJournalId;

        /**
         * @var string
         *
         */
        public $externalRecordId;

    }

}
