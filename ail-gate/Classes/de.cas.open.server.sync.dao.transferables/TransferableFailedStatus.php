<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableFailedStatus {

        /**
         * @var string
         *
         */
        public $failureMessage;

        /**
         * @var string
         *
         */
        public $failureType;

        /**
         * @var array
         *
         */
        public $syncViewIds;

    }

}
