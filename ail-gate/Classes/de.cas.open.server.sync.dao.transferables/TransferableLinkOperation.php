<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableLinkOperation {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableLink
         *
         */
        public $link;

        /**
         * @var string
         *
         */
        public $sourceRecordCheckSumAtSyncStart;

        /**
         * @var string
         *
         */
        public $targetRecordCheckSumAtSyncStart;

    }

}
