<?php
// This file has been automatically generated.

namespace de\cas\gw\server\questionaire\types {

    /**
     * @package de\cas\gw\server\questionaire
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the available survey templates for the given data object.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetSurveyTemplatesForDataObjectResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSurveyTemplatesForDataObjectResponse
     */
    class GetSurveyTemplatesForDataObjectRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The object type of the data object.
         */
        public $ObjectType;

        /**
         * @var string
         *
         *                    The guid of the data object.
         */
        public $Guid;

    }

}
