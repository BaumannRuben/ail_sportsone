<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Performs a Login at the GenesisWorld Server.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: LoginResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see LoginResponse
     */
    class LoginRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The username for the login.
         */
        public $Username;

        /**
         * @var string
         *
         *                    The password for the login.
         */
        public $Password;

        /**
         * @var string
         *
         *                    The servername for the login.
         */
        public $Servername;

        /**
         * @var string
         *
         *                    The password for the login.
         */
        public $Database;

        /**
         * @var string
         *
         *                    The language for the login, specified by the ISOCODE (e.g. "en", or with country code: e.g. "en-US").
         */
        public $Language;

        /**
         * @var array
         *
         *                    To specify custom settings for the login. This feature is not used until now.
         */
        public $Settings;

    }

}
