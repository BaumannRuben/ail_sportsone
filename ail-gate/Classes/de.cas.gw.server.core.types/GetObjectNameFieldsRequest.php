<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns a list of fields to use to represent an object
     *        with the given type for the given view type.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetObjectNameFieldsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetObjectNameFieldsResponse
     */
    class GetObjectNameFieldsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $ObjectType;

        /**
         * @var string
         *
         */
        public $ViewType;

    }

}
