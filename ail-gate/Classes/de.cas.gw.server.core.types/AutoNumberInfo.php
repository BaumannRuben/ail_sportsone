<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        Describes a auto numbering field.
     */
    class AutoNumberInfo {

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $fieldName;

        /**
         * @var boolean
         *
         */
        public $isAutoNumberGenerationEnabled;

    }

}
