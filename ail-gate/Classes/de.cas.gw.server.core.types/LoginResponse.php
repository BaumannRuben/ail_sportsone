<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Performs a Login at the GenesisWorld Server.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: LoginRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see LoginRequest
     */
    class LoginResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The returned sessionTicket for the login.
         */
        public $SessionTicket;

    }

}
