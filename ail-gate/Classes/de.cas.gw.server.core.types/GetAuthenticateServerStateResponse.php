<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the AuthenticateServerState.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetAuthenticateServerStateRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAuthenticateServerStateRequest
     */
    class GetAuthenticateServerStateResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         */
        public $authenticateServerState;

    }

}
