<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Checks if current user is allowed to use Smartaccess
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: IsSmartAccessAllowedResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see IsSmartAccessAllowedResponse
     */
    class IsSmartAccessAllowedRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
