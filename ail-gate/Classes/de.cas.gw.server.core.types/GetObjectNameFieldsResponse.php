<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns a list of field names to use to represent an object
     *        with the given type.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetObjectNameFieldsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetObjectNameFieldsRequest
     */
    class GetObjectNameFieldsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $Fields;

        /**
         * @var array
         *
         */
        public $InfoFields;

    }

}
