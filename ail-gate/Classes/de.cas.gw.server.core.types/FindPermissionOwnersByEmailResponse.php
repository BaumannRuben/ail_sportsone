<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        Contains the permission owners (users/resources/groups) that match the email provided in the request.
     */
    class FindPermissionOwnersByEmailResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    The permission owners that match the given email address.
         */
        public $matchingPermissionOwners;

    }

}
