<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the a list of AutoNumberInfo for the given object types.
     *        \de\cas\open\server\api\types\ResponseObject: GetAutoNumberFieldsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AutoNumberInfo
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAutoNumberFieldsResponse
     */
    class GetAutoNumberFieldsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         */
        public $objectTypes;

    }

}
