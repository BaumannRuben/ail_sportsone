<?php
// This file has been automatically generated.

namespace de\cas\open\server\systemnews\types {

    /**
     * @package de\cas\open\server\systemnews
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Confirms the system news with
     *        the given guids.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: ConfirmSystemNewsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ConfirmSystemNewsResponse
     */
    class ConfirmSystemNewsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *Gets/sets guids of the news to be confirmed.
         */
        public $Guids;

        /**
         * @var string
         *Gets/sets the client prefix.
         */
        public $clientPrefix;

    }

}
