<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\connectionbridge {

    /**
     * @package de\cas\open\server\sync
     * @subpackage connectionbridge
     *
     */
    class GetRequestedDataIdsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         */
        public $pageStart;

        /**
         * @var int
         *
         */
        public $pageCount;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncSessionContext
         *
         */
        public $syncSessionContext;

    }

}
