<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\connectionbridge {

    /**
     * @package de\cas\open\server\sync
     * @subpackage connectionbridge
     *
     */
    class UpdateRecurrenceSyncRecordsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $periodGuid;

        /**
         * @var int
         *
         */
        public $journalId;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableRecordId
         *
         */
        public $foreignRecordId;

        /**
         * @var array
         *
         */
        public $createData;

        /**
         * @var array
         *
         */
        public $updateData;

        /**
         * @var array
         *
         */
        public $deleteData;

        /**
         * @var array
         *
         */
        public $syncViewDefinitions;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncSessionContext
         *
         */
        public $syncSessionContext;

    }

}
