<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains the fallback
     *				view definition. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetFallbackViewDefinitionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFallbackViewDefinitionRequest
     */
    class GetFallbackViewDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										The non-persisted ViewDefinition that was created
         *										for the given viewType.
         */
        public $viewDefinition;

    }

}
