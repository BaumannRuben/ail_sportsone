<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     * \de\cas\open\server\api\types\ResponseObject: Returns the sort
     *				index
     *				for each viewId.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				GetViewDefinitionsSortOrderRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetViewDefinitionsSortOrderRequest
     */
    class GetViewDefinitionsSortOrderResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *A list of key-value pairs with the viewId as key
         *										and the corresponding sort-index as value.
         */
        public $viewSortIndexes;

    }

}
