<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Creates a non-persisted
     *				ViewDefinition that may be used as a fallback ViewDefinition in case
     *				a user does not see any ViewDefinitions. This is guaranteed to work
     *				for the main object-types (ADDRESS, APPOINTMENT, ...) without a
     *				certain view scope and depends on the servers configuration for
     *				other view types like MIXEDLIST. <br/>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetFallbackViewDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFallbackViewDefinitionResponse
     */
    class GetFallbackViewDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The type of the view to be returned (e.g.
         *										ADDRESS, APPOINTMENT, TODO, ...).
         *										It is guaranteed that the
         *										server returns a reasonable view definition for the main
         *										object-types but it depends on its configuration if a view
         *										definition can be built for other view types like MIXEDLIST.
         */
        public $viewType;

        /**
         * @var string
         *
         *										The viewScope of the view to be returned.
         *										(optional)
         *										If the server knows about a certain view scope
         *										depends on its configuration.
         */
        public $viewScope;

    }

}
