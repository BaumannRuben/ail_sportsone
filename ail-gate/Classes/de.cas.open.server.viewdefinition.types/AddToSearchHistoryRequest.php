<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Adds a ViewDefinition
     *				as search history item.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: AddToSearchHistoryResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AddToSearchHistoryResponse
     */
    class AddToSearchHistoryRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										Sets/Returns the view definition that should be
         *										added
         *										as search history item.
         */
        public $viewDefinition;

        /**
         * @var int
         *
         *										Defines the amount of search history items to be
         *										kept. (optional, default to 10)
         */
        public $searchHistorySize;

        /**
         * @var string
         *
         *										Defines for which search history items group the
         *										search history size should be forced upon.
         */
        public $evictionPolicy;

    }

}
