<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *					This class represents a definition of a view. A view defines which fields are
     *					displayed in a filtered list, which sizes of the columns where configured by
     *					the user, which sorting was set, ... Each ViewDefinition has a 1:1 relationship
     *					with a DataObject of type Filter. The Filter defines, which data is selected
     *					and from where this data is selected (from Addresses, Appointments,
     *					Opportunities, ... or even from a MixedList).
     *					<br/>
     *					The viewID must not exceed a maximum length of 200 chars.
     */
    class ViewDefinition {

        /**
         * @var string
         *
         *									The name of the view. The name is not unique. To uniquely
         *									itdentiy a view, use the {#viewId} property.
         *									<br/>
         *									The viewName will be translated by the server if the ViewDefinition
         *									is defined by the system. In case of a user-created ViewDefinition the viewName
         *									equals the {#viewId}. The server only looks at the {#viewId} and discards the viewName, so
         *									use the {#viewName} only for display purposes.
         */
        public $viewName;

        /**
         * @var string
         *
         *								The ID of the view.
         *								<br/>
         *								<br/>
         *								<strong>The viewID must not exceed a maximum length of 32 chars.</strong>
         */
        public $viewId;

        /**
         * @var string
         *
         *								An identifier that indicates the type this view
         *								results in (e.g. ADDRESS,
         *								APPOINTMENT, ..., MIXEDLIST).
         */
        public $viewType;

        /**
         * @var string
         *
         *								An identifier that can be set by the client to
         *								distinguish views depending on the scope they are used in, e.g.
         *								DOSSIER, RECYCLEBIN, SEARCHRESULT, ...
         */
        public $viewScope;

        /**
         * @var string
         *
         *                Indicates the kind of the view, e.g. list, history
         */
        public $viewKind;

        /**
         * @var boolean
         *
         *								Flags a ViewDefinition as deletable by the user or
         *								not. A
         *								ViewDefinition is not deletable, if it was defined by the
         *								system or by an admin. This is a read-only field, it will be
         *								ignored by the server when a ViewDefinition is being saved.
         */
        public $deletable;

        /**
         * @var boolean
         *
         *								Flags a ViewDefinition as a default-ViewDefinition.
         *								A
         *								default-ViewDefinition is used as the main-ViewDefinition for a
         *								view-type. Newly defined filters will be assigned to such a
         *								default-ViewDefinition. For every view-type, there exists one
         *								default-ViewDefinition. This is a read-only field, it will be
         *								ignored by the server when a ViewDefinition is being saved.
         */
        public $systemDefault;

        /**
         * @var boolean
         *
         *								Indicates if a view definition is system defined.
         */
        public $systemDefined;

        /**
         * @var boolean
         *
         *								Flags a ViewDefinition as a default-ViewDefinition
         *								for a user.
         *								This ViewDefinition will be the one that will be used
         *								initially
         *								when opening an area in the navigator.
         */
        public $userDefault;

        /**
         * @var array
         *
         *								A list containing the columns, that will be
         *								displayed to the
         *								user and will be used in the query that is sent
         *								to the server.
         */
        public $selectColumns;

        /**
         * @var array
         *
         *								A list containing information about the the
         *								sort-order of the
         *								columns defined in the field
         *								<code>selectColumns</code>.
         */
        public $orderColumns;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *								The filter-dataobject that is assigned to this
         *								ViewDefinition. May be null.
         */
        public $referencedFilter;

        /**
         * @var int
         *
         *								The index for sorting ViewDefinitions in the GUI. 0
         *								is the
         *								top-most index.
         */
        public $sortIndex;

        /**
         * @var boolean
         *
         *								Flags a ViewDefinition as being public available or
         *								not.
         */
        public $publicView;

        /**
         * @var string
         *
         *								Sets/Returns the new foreign edit permission
         *								restriction (i.e.
         *								the maximum permission a user can inherit by
         *								foreign edit
         *								permission).
         */
        public $foreignEditPermissionRestriction;

        /**
         * @var array
         *A list of DataObjectPermissions that apply to the
         *								view.
         */
        public $permissions;

    }

}
