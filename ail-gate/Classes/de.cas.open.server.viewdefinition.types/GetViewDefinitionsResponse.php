<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: TODO: Purpose of the
     *				business logic. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetViewDefinitionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetViewDefinitionsRequest
     */
    class GetViewDefinitionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										The ViewDefinitions that were found for the view
         *										type.
         */
        public $viewDefinitions;

    }

}
