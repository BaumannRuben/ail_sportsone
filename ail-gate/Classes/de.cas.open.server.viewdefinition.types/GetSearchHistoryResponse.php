<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains the search
     *				history of the logged in user.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetSearchHistoryRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSearchHistoryRequest
     */
    class GetSearchHistoryResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Sets/Returns the view definitions sorted by their
         *										insertion date (descending).
         */
        public $viewDefinitions;

    }

}
