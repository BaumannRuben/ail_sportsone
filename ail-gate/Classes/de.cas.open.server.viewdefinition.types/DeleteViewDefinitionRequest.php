<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Requests to delete a
     *				view and the linked filter.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteViewDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteViewDefinitionResponse
     */
    class DeleteViewDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *The ID of the view to be deleted.
         */
        public $viewId;

    }

}
