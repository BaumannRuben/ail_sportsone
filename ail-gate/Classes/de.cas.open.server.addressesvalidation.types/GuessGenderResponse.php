<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				Response object for the business operation that
     *				guesses the gender, based on the first name.
     *				If no genderguesser plugIn is able to guess the gender, UNKNOWN is returned.
     *				A null value isn't returned (but a null check doesn't hurt).
     */
    class GuessGenderResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *Guessed gender of first name of the request.
         */
        public $gender;

    }

}
