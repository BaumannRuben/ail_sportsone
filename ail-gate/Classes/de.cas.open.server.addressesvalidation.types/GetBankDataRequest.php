<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject
     *        of the business operation that enriches a given \de\cas\open\server\addresses\validation\types\BankData with all
     *        further data that can be derived from the already contained.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetBankDataResponse.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\addresses\validation\types\BankData
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetBankDataResponse
     */
    class GetBankDataRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\addressesvalidation\types\BankData
         *
         *                    \de\cas\open\server\addresses\validation\types\BankData
         *                    container containing currently known bank data information.
         *	@see \de\cas\open\server\addresses\validation\types\BankData
         */
        public $bankData;

        /**
         * @var string
         *
         *                    The ISO code of the country. Mandatory.
         */
        public $countryCode;

    }

}
