<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				Request object for the business operation that
     *				guesses the gender, based on the first name.
     *				If no genderguesser plugIn is able to guess the gender, UNKNOWN is returned.
     *				A null value isn't returned (but a null check doesn't hurt).
     */
    class GuessGenderRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Gets the user to be saved.
         */
        public $firstName;

    }

}
