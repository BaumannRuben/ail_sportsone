<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        Container for bank data.
     */
    class BankData {

        /**
         * @var string
         *
         */
        public $bic;

        /**
         * @var string
         *
         */
        public $bankCode;

        /**
         * @var string
         *
         */
        public $bankName;

    }

}
