<?php
// This file has been automatically generated.

namespace de\cas\gw\server\reports\types {

    /**
     * @package de\cas\gw\server\reports
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the templates for the given object type.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetReportTemplatesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetReportTemplatesResponse
     */
    class GetReportTemplatesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The object type for the templates.
         */
        public $ObjectType;

    }

}
