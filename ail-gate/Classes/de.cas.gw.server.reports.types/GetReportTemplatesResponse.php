<?php
// This file has been automatically generated.

namespace de\cas\gw\server\reports\types {

    /**
     * @package de\cas\gw\server\reports
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the templates for the given object type.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetReportTemplatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetReportTemplatesRequest
     */
    class GetReportTemplatesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *                    The MassQueryResult with templates.
         */
        public $ReportTemplates;

    }

}
