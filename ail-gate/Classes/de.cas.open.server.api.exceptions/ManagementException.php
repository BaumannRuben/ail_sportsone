<?php

namespace de\cas\open\server\api\exceptions {

    /**
     * This class indicates exceptions in a management operation. The reason for this exception can be determined using its property {@link #getDetailCode()}.
     *
     * @package de\cas\open\server\api
     * @subpackage exceptions
     */
    class ManagementException extends EimException {

    }
}
