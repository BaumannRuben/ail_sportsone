<?php

namespace de\cas\open\server\api\exceptions {

    /**
     * This class is indicates exceptions in the datalayer. The reason for this exception can be determined using its property {@link #getDetailCode()}.
     *
     * @package de\cas\open\server\api
     * @subpackage exceptions
     */
    class DataLayerException extends EimException {

    }
}
