<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the E-Mail addresses and names from the TO and
     *        CC fields of an archived E-Mail. Corresponding \de\cas\open\server\api\types\RequestObject: GetEMailRecipientsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetEMailRecipientsRequest
     */
    class GetEMailRecipientsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Gets the reference to the TO list
         */
        public $TO;

        /**
         * @var array
         *
         *                    Gets the reference to the CC list
         */
        public $CC;

        /**
         * @var array
         *
         *                    Gets the reference to the BCC list
         */
        public $BCC;

    }

}
