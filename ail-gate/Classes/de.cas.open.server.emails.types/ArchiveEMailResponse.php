<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Creates a document from an EMail and archives it.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: ArchiveEMailRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ArchiveEMailRequest
     */
    class ArchiveEMailResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the archived eMail document.
         */
        public $GGUID;

    }

}
