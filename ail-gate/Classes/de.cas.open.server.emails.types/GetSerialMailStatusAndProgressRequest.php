<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the send status and progress of the serial E-Mail of the given gguid.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetSerialMailStatusAndProgressResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSerialMailStatusAndProgressResponse
     */
    class GetSerialMailStatusAndProgressRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the serial e-mail object which status shall be returned.
         */
        public $GGUID;

    }

}
