<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the send progress of the serial E-Mail of the given gguid.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetSerialMailProgressRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSerialMailProgressRequest
     */
    class GetSerialMailProgressResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var int
         *
         *                    Gets the count of all emails.
         */
        public $totalEmailsCount;

        /**
         * @var int
         *
         *                    Gets the count of the sent emails.
         */
        public $sentEmailsCount;

        /**
         * @var int
         *
         *                    Gets the count of the emails, that couldn't be sent.
         */
        public $failedEmailsCount;

    }

}
