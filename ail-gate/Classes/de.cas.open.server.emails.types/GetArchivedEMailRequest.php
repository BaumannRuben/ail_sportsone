<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns an archived EMail for a given GGUID of an
     *        EMailStore object. Corresponding \de\cas\open\server\api\types\ResponseObject: GetArchivedEMailResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetArchivedEMailResponse
     */
    class GetArchivedEMailRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the eMailStore object
         */
        public $eMailStoreGGUID;

        /**
         * @var int
         *
         *                    Determines what should be filtered(IFRAME, onClick, IMG
         *                    src="...", ...). Please see \de\cas\eim\api\constants\HtmlFilterConstants.
         *	@see \de\cas\eim\api\constants\HtmlFilterConstants
         */
        public $filterValue;

    }

}
