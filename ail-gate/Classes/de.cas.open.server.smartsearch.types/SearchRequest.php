<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Does a search.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: SearchResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SearchResponse
     */
    class SearchRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *Sets/Gets the search term.
         */
        public $Term;

        /**
         * @var array
         *Object types to search.
         */
        public $DataObjectTypes;

        /**
         * @var unknown
         *Specifies the result page to retrieve.
         */
        public $Page;

        /**
         * @var unknown
         *Specifies the number of results in a page.
         */
        public $PageSize;

    }

}
