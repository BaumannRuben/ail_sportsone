<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns all countries, states and regions that have
     *			public holidays stored in the database.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: GetAvailableCountriesStatesRegionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAvailableCountriesStatesRegionsRequest
     */
    class GetAvailableCountriesStatesRegionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Sets/Gets the list of PublicHolidayInfo objects
         *										holding
         *										the countries, states and regions.
         */
        public $availableCSRs;

    }

}
