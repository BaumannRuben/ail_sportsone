<?php

#Including AIL-gate
require __DIR__ . "/../AILGate.php";

$appointment = HochwarthIT_AILGate::getEIMInterface()->createObject('APPOINTMENT');
$appointment->setValue("Test Appointment", "KEYWORD", "STRING");
$appointment->setValue(FALSE, "ISPARTOFEVENT", "BOOLEAN");
$appointment = HochwarthIT_AILGate::getEIMInterface()->saveAndReturnObject($appointment);