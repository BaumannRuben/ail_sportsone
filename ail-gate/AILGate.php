<?php
require_once dirname(__FILE__)."/SecurityTokenProvider.php";
require_once dirname(__FILE__)."/Dao/EventDao.php";
require_once dirname(__FILE__)."/Dao/AddressDao.php";
require_once dirname(__FILE__)."/Dao/AppointmentDao.php";
require_once dirname(__FILE__)."/Dao/RegistrationDao.php";
require_once dirname(__FILE__)."/Dao/BillDao.php";
require_once dirname(__FILE__)."/Dao/MannschaftDao.php";
require_once dirname(__FILE__)."/Dao/DocumentDao.php";
require_once dirname(__FILE__)."/Dao/TodoDao.php";
require_once dirname(__FILE__)."/AILGate/InputAssistance/Address/Type.php";

class HochwarthIT_AILGate{

    static function getEIMInterface(){
        return SecurityTokenProvider::getInstance()->getEIMInterface();
    }

    static function getAvailableObjects(){
        return self::getEIMInterface()->getAvailableObjectNames();
    }

    static function getAdressDao(){
        return AddressDao::getInstance();
    }

    static function getEventDao(){
        return EventDao::getInstance();
    }

    static function getAppointmentDao(){
        return AppointmentDao::getInstance();
    }

    static function getRegistrationDao(){
        return RegistrationDao::getInstance();
    }

    static function getBillDao(){
        return BillDao::getInstance();
    }
    
    static function getMannschaftDao(){
    	return MannschaftDao::getInstance();
    }
    static function getDocumentDao(){
    	return DocumentDao::getInstance();
    }
    static function getTodoDao(){
    	return TodoDao::getInstance();
    }
}