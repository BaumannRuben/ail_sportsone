<?php
class SportsOneApi {
    //LIVE-SERVER
    private $host   = 'https://ail2001sportsone.hana.ondemand.com'; //Alte Url: https://sapsportsonea67310256.hana.ondemand.com
    private $token;
    private $login;
    

	 /**
     * Example1 hooks into the pages->save method and displays a notice every time a page is saved
     * @param null $service_url
     * @param null $userData
     * @param null $httpVerb
     * @return mixed
     * @internal param null $user
     */
    function __construct() {
        
        $this->setLogin();
        $this->setToken();
    }
    
	private function sendRequest($service_url, $header, $data='', $httpVerb = 'post') {
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_VERBOSE, true);
	    curl_setopt($ch, CURLOPT_URL, $this->host.$service_url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    
	    if($httpVerb == 'GET'){
	        curl_setopt($ch, CURLOPT_HTTPGET, true);
	    }
	    
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_HEADER , 1);
	    
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    
	    //todo data
	    if(strlen($data) > 0){
	        curl_setopt($ch, CURLOPT_POSTFIELDS,  $data);
	    }
	    
	    $response = curl_exec($ch);
	    
	    curl_close($ch);

        return $response;
	}
	
	private function setToken(){
	    
	    $service_url = "/sap/sports/fnd/db/services/public/xs/token.xsjs";
	    $header = Array(
	        "Content-Type: application/json",
	        "Accept: application/json",
	        "X-CSRF-Token: Fetch",
	        "Cookie: ".$this->login
	        );
	    
	    // Send the request & save response to $response
	    $response = $this->sendRequest($service_url, $header, '', 'get');
	    
	    $start = strpos($response, 'x-csrf-token:');
	    $token = substr($response, $start+14, 32);
	    #echo "Token: ".$token."<br><br>";
	    
	    $this->token = $token;  
	}
	
	private function setLogin(){
	    $service_url = "/sap/hana/xs/formLogin/login.xscfunc";

	    $header = array(
	        "x-csrf-token: unsafe"
	    );
	    
	    $data = 'xs-username=BAUMANNR&xs-password=AiL2017casSAP';
	    $response = $this->sendRequest($service_url, $header, $data);
	   
	    $cookies = array();
	    preg_match_all('/set-cookie:(?<cookie>\s{0,}.*)$/im', $response, $cookies);

	    $result = str_replace(' ','', $cookies['cookie'][0]).';'.str_replace(' ','', $cookies['cookie'][1]);
	    $result = str_replace("\n", '', $result);
	    $result = str_replace("\r", '', $result);
	    
	    $this->login = $result; 
	}
	
	public function importPerson($data){
	    $service_url = "/sap/sports/imp/api/copyQueue/service/rest/copyQueue/PERSON/v1";
	    $header = array(
	        "Content-Type: application/json",
 	        "Authorization: Basic Og==",
	        "X-CSRF-Token: ".$this->token,
	        "Cookie: ".$this->login
	        
	    );
	  	    	    
	    // Send the request & save response to $resp
	    $response = $this->sendRequest($service_url, $header, $data);
	    //print_r($response);
	    	  
	    return $response;
	}
	
	public function importPersonAddress($data){
	    $service_url = "/sap/sports/imp/api/copyQueue/service/rest/copyQueue/ADDRESS/v1";
	    $header = array(
	        "Content-Type: application/json",
	        "Authorization: Basic Og==",
	        "X-CSRF-Token: ".$this->token,
	        "Cookie: ".$this->login
	    );
	    $response = $this->sendRequest($service_url, $header, $data);
	    return $response;
	}
	public function importPersonCommunication($data){
	    $service_url = "/sap/sports/imp/api/copyQueue/service/rest/copyQueue/COMMUNICATION/v1";
	    $header = array(
	        "Content-Type: application/json",
	        "Authorization: Basic Og==",
	        "X-CSRF-Token: ".$this->token,
	        "Cookie: ".$this->login
	    );
	    $response = $this->sendRequest($service_url, $header, $data);
	    return $response;
	}
	
	public function importTeam($data){
	    $service_url = "/sap/sports/imp/api/copyQueue/service/rest/copyQueue/TEAM/v1";
	    $header = array(
	        "Content-Type: application/json",
	        "Authorization: Basic Og==",
	        "X-CSRF-Token: ".$this->token,
	        "Cookie: ".$this->login
	        
	    );
	    
	    // Send the request & save response to $resp
	    $response = $this->sendRequest($service_url, $header, $data);
	    //print_r($response);
	    
	    return $response;
	}
	
	public function importTeamMember($data){
	    $service_url = "/sap/sports/imp/api/copyQueue/service/rest/copyQueue/TEAM_MEMBER/v1";
	    
	    $header = array(
	        "Content-Type: application/json",
	        "Authorization: Basic Og==",
	        "X-CSRF-Token: ".$this->token,
	        "Cookie: ".$this->login
	        
	    );
	    
	    // Send the request & save response to $resp
	    $response = $this->sendRequest($service_url, $header, $data);
	    //print_r($response);
	    
	    return $response;
	}
	
}